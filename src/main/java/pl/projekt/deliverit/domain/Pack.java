package pl.projekt.deliverit.domain;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Pack.
 */
@Entity
@Table(name = "pack")
@Document(indexName = "pack")
public class Pack implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "package_code")
    private String packageCode;

    @ManyToOne
    private PackageSlot packageSlot;

    @ManyToOne
    private TransitPoint endTrasitPoint;

    @ManyToOne
    private ShipmentOfferSubmition shipmentOfferSubmition;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    public PackageSlot getPackageSlot() {
        return packageSlot;
    }

    public void setPackageSlot(PackageSlot packageSlot) {
        this.packageSlot = packageSlot;
    }

    public TransitPoint getEndTrasitPoint() {
        return endTrasitPoint;
    }

    public void setEndTrasitPoint(TransitPoint transitPoint) {
        this.endTrasitPoint = transitPoint;
    }

    public ShipmentOfferSubmition getShipmentOfferSubmition() {
        return shipmentOfferSubmition;
    }

    public void setShipmentOfferSubmition(ShipmentOfferSubmition shipmentOfferSubmition) {
        this.shipmentOfferSubmition = shipmentOfferSubmition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pack pack = (Pack) o;
        if(pack.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, pack.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Pack{" +
            "id=" + id +
            ", packageCode='" + packageCode + "'" +
            '}';
    }
}
