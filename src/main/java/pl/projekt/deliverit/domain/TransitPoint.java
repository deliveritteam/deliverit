package pl.projekt.deliverit.domain;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A TransitPoint.
 */
@Entity
@Table(name = "transit_point")
@Document(indexName = "transitpoint")
public class TransitPoint implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "city")
    private String city;

    @Column(name = "date")
    private ZonedDateTime date;

    @ManyToOne
    private ShipmentOffer shipmentOffer;

    @ManyToOne
    private Place place;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public ShipmentOffer getShipmentOffer() {
        return shipmentOffer;
    }

    public void setShipmentOffer(ShipmentOffer shipmentOffer) {
        this.shipmentOffer = shipmentOffer;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TransitPoint transitPoint = (TransitPoint) o;
        if(transitPoint.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, transitPoint.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TransitPoint{" +
            "id=" + id +
            ", city='" + city + "'" +
            ", date='" + date + "'" +
            '}';
    }
}
