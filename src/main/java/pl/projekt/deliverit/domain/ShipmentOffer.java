package pl.projekt.deliverit.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A ShipmentOffer.
 */
@Entity
@Table(name = "shipment_offer")
@Document(indexName = "shipmentoffer")
public class ShipmentOffer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "description")
    private String description;

    @ManyToOne
    private User user_shipment;

    @OneToMany(mappedBy = "shipmentOffer")
    private Set<TransitPoint> transitPoints = new HashSet<>();

    @OneToMany(mappedBy = "shipmentOffer")
    @JsonIgnore
    private Set<PackageSlot> packageSlots = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser_shipment() {
        return user_shipment;
    }

    public void setUser_shipment(User user) {
        this.user_shipment = user;
    }

    public Set<TransitPoint> getTransitPoints() {
        return transitPoints;
    }

    public void setTransitPoints(Set<TransitPoint> transitPoints) {
        this.transitPoints = transitPoints;
    }

    public Set<PackageSlot> getPackageSlots() {
        return packageSlots;
    }

    public void setPackageSlots(Set<PackageSlot> packageSlots) {
        this.packageSlots = packageSlots;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ShipmentOffer shipmentOffer = (ShipmentOffer) o;
        if(shipmentOffer.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, shipmentOffer.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ShipmentOffer{" +
            "id=" + id +
            ", date='" + date + "'" +
            ", description='" + description + "'" +
            '}';
    }
}
