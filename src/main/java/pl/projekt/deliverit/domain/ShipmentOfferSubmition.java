package pl.projekt.deliverit.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import pl.projekt.deliverit.domain.enumeration.Status;

/**
 * A ShipmentOfferSubmition.
 */
@Entity
@Table(name = "shipment_offer_submition")
@Document(indexName = "shipmentoffersubmition")
public class ShipmentOfferSubmition implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status;

    @NotNull
    @Size(max = 264)
    @Column(name = "message", length = 264, nullable = false)
    private String message;

    @Column(name = "date")
    private LocalDate date;

    @NotNull
    @Column(name = "receip_date", nullable = false)
    private LocalDate receipDate;

    @ManyToOne
    private TransitPoint startTransitPoint;

    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "shipmentOfferSubmition")
    @JsonIgnore
    private Set<Pack> packagess = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalDate getReceipDate() {
        return receipDate;
    }

    public void setReceipDate(LocalDate receipDate) {
        this.receipDate = receipDate;
    }

    public TransitPoint getStartTransitPoint() {
        return startTransitPoint;
    }

    public void setStartTransitPoint(TransitPoint transitPoint) {
        this.startTransitPoint = transitPoint;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Pack> getPackagess() {
        return packagess;
    }

    public void setPackagess(Set<Pack> packs) {
        this.packagess = packs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ShipmentOfferSubmition shipmentOfferSubmition = (ShipmentOfferSubmition) o;
        if(shipmentOfferSubmition.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, shipmentOfferSubmition.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ShipmentOfferSubmition{" +
            "id=" + id +
            ", status='" + status + "'" +
            ", message='" + message + "'" +
            ", date='" + date + "'" +
            ", receipDate='" + receipDate + "'" +
            '}';
    }
}
