package pl.projekt.deliverit.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import pl.projekt.deliverit.domain.enumeration.PlaceStatus;

/**
 * A Place.
 */
@Entity
@Table(name = "place")
@Document(indexName = "place")
public class Place implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Size(max = 60)
    @Column(name = "city", length = 60, nullable = false)
    private String city;

    @Column(name = "latitude")
    private Float latitude;

    @Column(name = "longitude")
    private Float longitude;

    @NotNull
    @Size(max = 60)
    @Column(name = "address", length = 60, nullable = false)
    private String address;

    @Column(name = "creation_date")
    private ZonedDateTime creationDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private PlaceStatus status;

    @NotNull
    @Size(max = 10)
    @Column(name = "post_code", length = 10, nullable = false)
    private String postCode;

    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "place")
    @JsonIgnore
    private Set<TransitPoint> transitPoints = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ZonedDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public PlaceStatus getStatus() {
        return status;
    }

    public void setStatus(PlaceStatus status) {
        this.status = status;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<TransitPoint> getTransitPoints() {
        return transitPoints;
    }

    public void setTransitPoints(Set<TransitPoint> transitPoints) {
        this.transitPoints = transitPoints;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Place place = (Place) o;
        if(place.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, place.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Place{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", city='" + city + "'" +
            ", latitude='" + latitude + "'" +
            ", longitude='" + longitude + "'" +
            ", address='" + address + "'" +
            ", creationDate='" + creationDate + "'" +
            ", status='" + status + "'" +
            ", postCode='" + postCode + "'" +
            '}';
    }
}
