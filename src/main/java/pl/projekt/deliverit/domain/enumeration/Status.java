package pl.projekt.deliverit.domain.enumeration;

/**
 * The Status enumeration.
 */
public enum Status {
    messageSent, approved, received, abandoned, receivedProblem, deliverProblem, delivered
}
