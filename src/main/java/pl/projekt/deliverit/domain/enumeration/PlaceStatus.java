package pl.projekt.deliverit.domain.enumeration;

/**
 * The PlaceStatus enumeration.
 */
public enum PlaceStatus {
    pending,approved, disapproved
}
