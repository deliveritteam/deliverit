package pl.projekt.deliverit.domain;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A PackageSlot.
 */
@Entity
@Table(name = "package_slot")
@Document(indexName = "packageslot")
public class PackageSlot implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(max = 30)
    @Column(name = "size", length = 30, nullable = false)
    private String size;

    @Column(name = "price")
    private Float price;

    @Column(name = "quantity")
    private Integer quantity;

    @NotNull
    @Column(name = "remaining_quantity", nullable = false)
    private Integer remainingQuantity;

    @ManyToOne
    private ShipmentOffer shipmentOffer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getRemainingQuantity() {
        return remainingQuantity;
    }

    public void setRemainingQuantity(Integer remainingQuantity) {
        this.remainingQuantity = remainingQuantity;
    }

    public ShipmentOffer getShipmentOffer() {
        return shipmentOffer;
    }

    public void setShipmentOffer(ShipmentOffer shipmentOffer) {
        this.shipmentOffer = shipmentOffer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PackageSlot packageSlot = (PackageSlot) o;
        if(packageSlot.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, packageSlot.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PackageSlot{" +
            "id=" + id +
            ", size='" + size + "'" +
            ", price='" + price + "'" +
            ", quantity='" + quantity + "'" +
            ", remainingQuantity='" + remainingQuantity + "'" +
            '}';
    }
}
