package pl.projekt.deliverit.service.impl;

import pl.projekt.deliverit.domain.enumeration.PlaceStatus;
import pl.projekt.deliverit.service.PlaceService;
import pl.projekt.deliverit.domain.Place;
import pl.projekt.deliverit.repository.PlaceRepository;
import pl.projekt.deliverit.repository.search.PlaceSearchRepository;
import pl.projekt.deliverit.web.rest.dto.PlaceDTO;
import pl.projekt.deliverit.web.rest.mapper.PlaceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Place.
 */
@Service
@Transactional
public class PlaceServiceImpl implements PlaceService{

    private final Logger log = LoggerFactory.getLogger(PlaceServiceImpl.class);

    @Inject
    private PlaceRepository placeRepository;

    @Inject
    private PlaceMapper placeMapper;

    @Inject
    private PlaceSearchRepository placeSearchRepository;

    /**
     * Save a place.
     *
     * @param placeDTO the entity to save
     * @return the persisted entity
     */
    public PlaceDTO save(PlaceDTO placeDTO) {
        log.debug("Request to save Place : {}", placeDTO);
        Place place = placeMapper.placeDTOToPlace(placeDTO);
        place = placeRepository.save(place);
        PlaceDTO result = placeMapper.placeToPlaceDTO(place);
        placeSearchRepository.save(place);
        return result;
    }

    /**
     *  Get all the places.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Place> findAll(Pageable pageable) {
        log.debug("Request to get all Places");
        Page<Place> result = placeRepository.findAll(pageable);
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PlaceDTO> findAllByStatus(PlaceStatus placeStatus) {
        log.debug("Request to get all Places with status : {}", placeStatus.toString());
        List<Place> result = placeRepository.findByStatus(placeStatus);
        List<PlaceDTO> places= new ArrayList<>();
        result.parallelStream().forEach(place -> places.add(placeMapper.placeToPlaceDTO(place)));
        return places;
    }

    /**
     *  Get one place by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PlaceDTO findOne(Long id) {
        log.debug("Request to get Place : {}", id);
        Place place = placeRepository.findOne(id);
        PlaceDTO placeDTO = placeMapper.placeToPlaceDTO(place);
        return placeDTO;
    }

    /**
     *  Delete the  place by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Place : {}", id);
        placeRepository.delete(id);
        placeSearchRepository.delete(id);
    }

    /**
     * Search for the place corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Place> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Places for query {}", query);
        return placeSearchRepository.search(queryStringQuery(query), pageable);
    }
}
