package pl.projekt.deliverit.service.impl;

import pl.projekt.deliverit.domain.TransitPoint;
import pl.projekt.deliverit.domain.User;
import pl.projekt.deliverit.repository.TransitPointRepository;
import pl.projekt.deliverit.service.PlaceService;
import pl.projekt.deliverit.service.ShipmentOfferService;
import pl.projekt.deliverit.domain.ShipmentOffer;
import pl.projekt.deliverit.repository.ShipmentOfferRepository;
import pl.projekt.deliverit.repository.search.ShipmentOfferSearchRepository;
import pl.projekt.deliverit.service.TransitPointService;
import pl.projekt.deliverit.service.UserService;
import pl.projekt.deliverit.web.rest.dto.PackageSlotDTO;
import pl.projekt.deliverit.web.rest.dto.ShipmentOfferDTO;
import pl.projekt.deliverit.web.rest.dto.TransitPointDTO;
import pl.projekt.deliverit.web.rest.mapper.PackageSlotMapper;
import pl.projekt.deliverit.web.rest.mapper.ShipmentOfferMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import pl.projekt.deliverit.web.rest.mapper.TransitPointMapper;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ShipmentOffer.
 */
@Service
@Transactional
public class ShipmentOfferServiceImpl implements ShipmentOfferService{

    private final Logger log = LoggerFactory.getLogger(ShipmentOfferServiceImpl.class);

    @Inject
    private ShipmentOfferRepository shipmentOfferRepository;

    @Inject
    private ShipmentOfferMapper shipmentOfferMapper;

    @Inject
    private TransitPointMapper transitPointMapper;

    @Inject
    private TransitPointService transitPointService;

    @Inject
    private PackageSlotMapper packageSlotMapper;

    @Inject
    private TransitPointRepository transitPointRepository;

    @Inject
    private ShipmentOfferSearchRepository shipmentOfferSearchRepository;

    /**
     * Save a shipmentOffer.
     *
     * @param shipmentOfferDTO the entity to save
     * @return the persisted entity
     */
    public ShipmentOfferDTO save(ShipmentOfferDTO shipmentOfferDTO) {
        log.debug("Request to save ShipmentOffer : {}", shipmentOfferDTO);
        ShipmentOffer shipmentOffer = shipmentOfferMapper.shipmentOfferDTOToShipmentOffer(shipmentOfferDTO);
        shipmentOffer = shipmentOfferRepository.save(shipmentOffer);
        ShipmentOfferDTO result = shipmentOfferMapper.shipmentOfferToShipmentOfferDTO(shipmentOffer);
        shipmentOfferSearchRepository.save(shipmentOffer);
        return result;
    }

    /**
     *  Get all the shipmentOffers.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentOffer> findAll(Pageable pageable) {
        log.debug("Request to get all ShipmentOffers");
        Page<ShipmentOffer> result = shipmentOfferRepository.findAll(pageable);
        return result;
    }

    @Transactional(readOnly = true)
    public List<ShipmentOfferDTO> findAllWithCities() {
        log.debug("Request to get all ShipmentOffers");
        List<ShipmentOfferDTO> response = new ArrayList<>();
        shipmentOfferRepository.findAll().
            stream().
            forEach(s -> response.add(mapToShipmentOfferDTO(s)));

        return response;
    }
    /**
     *  Get one shipmentOffer by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ShipmentOfferDTO findOneWithCity(Long id) {
        log.debug("Request to get ShipmentOffer with cities: {}", id);
        ShipmentOffer shipmentOffer = shipmentOfferRepository.findOne(id);
        ShipmentOfferDTO shipmentOfferDTO = mapToShipmentOfferDTO(shipmentOffer);
        return shipmentOfferDTO;
    }
    @Transactional(readOnly = true)
    public List<ShipmentOfferDTO> findAllForCities(String fromCity, String toCity){
        List<TransitPoint> fromCities = transitPointService.findByCity(fromCity);
        List<TransitPoint> toCities = transitPointService.findByCity(toCity);
        List<ShipmentOfferDTO> shipmentOffers = new ArrayList<>();
        if(fromCities==null||toCities==null){
            return shipmentOffers;
        }
        for(TransitPoint fromTransitPoint : fromCities){
            for(TransitPoint toTransitPoint: toCities){
                ShipmentOffer shipmentOffer = toTransitPoint.getShipmentOffer();
                if(fromTransitPoint.getShipmentOffer().getId().equals(shipmentOffer.getId())&&
                    fromTransitPoint.getDate().compareTo(toTransitPoint.getDate())<1 &&
                    !shipmentOffers.contains(shipmentOffer)){
                    shipmentOffers.add(mapToShipmentOfferDTO(shipmentOffer));
                }
            }
        }
        return shipmentOffers;
    }
    /**
     *  Get one shipmentOffer by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ShipmentOfferDTO findOne(Long id) {
        log.debug("Request to get ShipmentOffer : {}", id);
        ShipmentOffer shipmentOffer = shipmentOfferRepository.findOne(id);
        ShipmentOfferDTO shipmentOfferDTO = shipmentOfferMapper.shipmentOfferToShipmentOfferDTO(shipmentOffer);
        return shipmentOfferDTO;
    }
    @Transactional(readOnly = true)
    @Override
    public List<ShipmentOfferDTO> findAllForUser(){
        List<ShipmentOfferDTO> result = new ArrayList<>();
        shipmentOfferRepository.findByUser_shipmentIsCurrentUser().stream().
            forEach(shipmentOffer -> result.add(mapToShipmentOfferDTO(shipmentOffer)));
        return result;
    }

    /**
     *  Delete the  shipmentOffer by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ShipmentOffer : {}", id);
        shipmentOfferRepository.delete(id);
        shipmentOfferSearchRepository.delete(id);
    }

    /**
     * Search for the shipmentOffer corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentOffer> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ShipmentOffers for query {}", query);
        return shipmentOfferSearchRepository.search(queryStringQuery(query), pageable);
    }

    private ShipmentOfferDTO mapToShipmentOfferDTO(ShipmentOffer shipmentOffer){
        ShipmentOfferDTO shipmentOfferDTO = shipmentOfferMapper.shipmentOfferToShipmentOfferDTO(shipmentOffer);
        List<TransitPointDTO> transitPoints = new ArrayList<>();
        List<PackageSlotDTO> packageSlotDTOs = new ArrayList<>();

        shipmentOffer.getTransitPoints().stream().
            forEach(transitPoint ->  transitPoints.add(transitPointMapper.transitPointToTransitPointDTO(transitPoint)));

        shipmentOffer.getPackageSlots().stream().
            forEach(packageSlot -> packageSlotDTOs.add(packageSlotMapper.packageSlotToPackageSlotDTO(packageSlot)));

        shipmentOfferDTO.setPackageSlots(packageSlotDTOs);
        shipmentOfferDTO.transitPoints=transitPoints;
        return shipmentOfferDTO;
    }
}
