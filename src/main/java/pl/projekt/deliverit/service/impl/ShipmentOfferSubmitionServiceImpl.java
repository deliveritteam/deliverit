package pl.projekt.deliverit.service.impl;

import pl.projekt.deliverit.domain.*;
import pl.projekt.deliverit.repository.PackRepository;
import pl.projekt.deliverit.repository.ShipmentOfferRepository;
import pl.projekt.deliverit.service.ShipmentOfferService;
import pl.projekt.deliverit.service.ShipmentOfferSubmitionService;
import pl.projekt.deliverit.repository.ShipmentOfferSubmitionRepository;
import pl.projekt.deliverit.repository.search.ShipmentOfferSubmitionSearchRepository;
import pl.projekt.deliverit.service.UserService;
import pl.projekt.deliverit.web.rest.dto.PackDTO;
import pl.projekt.deliverit.web.rest.dto.ShipmentOfferSubmitionDTO;
import pl.projekt.deliverit.web.rest.mapper.PackMapper;
import pl.projekt.deliverit.web.rest.mapper.ShipmentOfferSubmitionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ShipmentOfferSubmition.
 */
@Service
@Transactional
public class ShipmentOfferSubmitionServiceImpl implements ShipmentOfferSubmitionService{

    private final Logger log = LoggerFactory.getLogger(ShipmentOfferSubmitionServiceImpl.class);

    @Inject
    private ShipmentOfferSubmitionRepository shipmentOfferSubmitionRepository;

    @Inject
    private PackRepository packRepository;

    @Inject
    private ShipmentOfferSubmitionMapper shipmentOfferSubmitionMapper;

    @Inject
    private ShipmentOfferRepository shipmentOfferRepository;
    @Inject
    private PackMapper packMapper;

    @Inject
    UserService userService;

    @Inject
    private ShipmentOfferSubmitionSearchRepository shipmentOfferSubmitionSearchRepository;

    /**
     * Save a shipmentOfferSubmition.
     *
     * @param shipmentOfferSubmitionDTO the entity to save
     * @return the persisted entity
     */
    public ShipmentOfferSubmitionDTO save(ShipmentOfferSubmitionDTO shipmentOfferSubmitionDTO) {
        log.debug("Request to save ShipmentOfferSubmition : {}", shipmentOfferSubmitionDTO);
        ShipmentOfferSubmition shipmentOfferSubmition = shipmentOfferSubmitionMapper.shipmentOfferSubmitionDTOToShipmentOfferSubmition(shipmentOfferSubmitionDTO);
        shipmentOfferSubmition = shipmentOfferSubmitionRepository.save(shipmentOfferSubmition);
        shipmentOfferSubmition =  shipmentOfferSubmitionSearchRepository.save(shipmentOfferSubmition);
        ShipmentOfferSubmitionDTO result = mapToShipmentOfferSubmitionDTO(shipmentOfferSubmition);
        return result;
    }

    /**
     *  Get all the shipmentOfferSubmitions.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentOfferSubmition> findAll(Pageable pageable) {
        log.debug("Request to get all ShipmentOfferSubmitions");
        Page<ShipmentOfferSubmition> result = shipmentOfferSubmitionRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one shipmentOfferSubmition by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ShipmentOfferSubmitionDTO findOne(Long id) {
        log.debug("Request to get ShipmentOfferSubmition : {}", id);
        ShipmentOfferSubmition shipmentOfferSubmition = shipmentOfferSubmitionRepository.findOne(id);
        ShipmentOfferSubmitionDTO shipmentOfferSubmitionDTO = mapToShipmentOfferSubmitionDTO(shipmentOfferSubmition);
        return shipmentOfferSubmitionDTO;
    }

    /**
     *  Delete the  shipmentOfferSubmition by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ShipmentOfferSubmition : {}", id);
        shipmentOfferSubmitionRepository.delete(id);
        shipmentOfferSubmitionSearchRepository.delete(id);
    }

    /**
     * Search for the shipmentOfferSubmition corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ShipmentOfferSubmition> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ShipmentOfferSubmitions for query {}", query);
        return shipmentOfferSubmitionSearchRepository.search(queryStringQuery(query), pageable);
    }

    @Transactional
    @Override
    public List<ShipmentOfferSubmitionDTO> findAllForCurrentUser(){
        List<ShipmentOfferSubmition> shipmentOfferSubmitions= shipmentOfferSubmitionRepository.findByUserIsCurrentUser();
        List<ShipmentOfferSubmitionDTO> result = new ArrayList<>();
        shipmentOfferSubmitions.stream().
            forEach(shipmentOfferSubmition -> result.add(mapToShipmentOfferSubmitionDTO(shipmentOfferSubmition)));
        return result;
    }
    @Transactional
    @Override
    public List<ShipmentOfferSubmitionDTO> findAllForCarrier() {
        List<ShipmentOfferSubmitionDTO> result= new ArrayList<>();
        shipmentOfferRepository.findByUser_shipmentIsCurrentUser().parallelStream().
            forEach(shipmentOffer -> result.addAll(findAllForShipmentOffer(shipmentOffer)));
        return result;
    }
    @Transactional
    private List<ShipmentOfferSubmitionDTO> findAllForShipmentOffer(ShipmentOffer shipmentOffer) {
        List<Pack> packs = new ArrayList<>();
        shipmentOffer.getPackageSlots().stream().
            forEach(packageSlot ->  packs.addAll(packRepository.findAllByPackageSlot(packageSlot)));
        List<ShipmentOfferSubmitionDTO> shipmentOfferSubmitions = new ArrayList<>();
        packs.stream().
            forEach(pack -> shipmentOfferSubmitions.add(mapToShipmentOfferSubmitionDTO(pack.getShipmentOfferSubmition())));
        return shipmentOfferSubmitions;
    }

    private ShipmentOfferSubmitionDTO mapToShipmentOfferSubmitionDTO(ShipmentOfferSubmition shipmentOfferSubmition){
        ShipmentOfferSubmitionDTO result = shipmentOfferSubmitionMapper.shipmentOfferSubmitionToShipmentOfferSubmitionDTO(shipmentOfferSubmition);
        ArrayList<PackDTO> packages = new ArrayList<>();
        shipmentOfferSubmition.getPackagess().stream().forEach(pack -> packages.add(packMapper.packToPackDTO(pack)));
        result.setPackagesDTOs(packages);
        return result;

    }
}
