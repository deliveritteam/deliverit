package pl.projekt.deliverit.service.impl;

import ch.qos.logback.classic.util.ContextInitializer;
import pl.projekt.deliverit.domain.Place;
import pl.projekt.deliverit.domain.enumeration.PlaceStatus;
import pl.projekt.deliverit.repository.PlaceRepository;
import pl.projekt.deliverit.repository.search.PlaceSearchRepository;
import pl.projekt.deliverit.service.TransitPointService;
import pl.projekt.deliverit.domain.TransitPoint;
import pl.projekt.deliverit.repository.TransitPointRepository;
import pl.projekt.deliverit.repository.search.TransitPointSearchRepository;
import pl.projekt.deliverit.web.rest.dto.TransitPointDTO;
import pl.projekt.deliverit.web.rest.mapper.TransitPointMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing TransitPoint.
 */
@Service
@Transactional
public class TransitPointServiceImpl implements TransitPointService{

    private final Logger log = LoggerFactory.getLogger(TransitPointServiceImpl.class);

    @Inject
    private TransitPointRepository transitPointRepository;

    @Inject
    private TransitPointMapper transitPointMapper;

    @Inject
    private PlaceRepository placeRepository;
    @Inject
    private TransitPointSearchRepository transitPointSearchRepository;

    /**
     * Save a transitPoint.
     *
     * @param transitPointDTO the entity to save
     * @return the persisted entity
     */
    public TransitPointDTO save(TransitPointDTO transitPointDTO) {
        log.debug("Request to save TransitPoint : {}", transitPointDTO);
        TransitPoint transitPoint = transitPointMapper.transitPointDTOToTransitPoint(transitPointDTO);
        transitPoint = transitPointRepository.save(transitPoint);
        TransitPointDTO result = transitPointMapper.transitPointToTransitPointDTO(transitPoint);
        transitPointSearchRepository.save(transitPoint);
        return result;
    }

    /**
     *  Get all the transitPoints.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<TransitPointDTO> findAll() {
        log.debug("Request to get all TransitPoints");
        List<TransitPointDTO> result = transitPointRepository.findAll().stream()
            .map(transitPointMapper::transitPointToTransitPointDTO)
            .collect(Collectors.toCollection(LinkedList::new));
        return result;
    }

    /**
     *  Get one transitPoint by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public TransitPointDTO findOne(Long id) {
        log.debug("Request to get TransitPoint : {}", id);
        TransitPoint transitPoint = transitPointRepository.findOne(id);
        TransitPointDTO transitPointDTO = transitPointMapper.transitPointToTransitPointDTO(transitPoint);
        return transitPointDTO;
    }

    @Override
    @Transactional(readOnly = true)
    public List<TransitPoint> findByCity(String city){
        List<TransitPoint> transitPoints = new ArrayList<>();
        List<Place> places = placeRepository.findByCityContainingIgnoreCase(city);
        log.debug("find places : {}", places.size());
        places.parallelStream().
            forEach(place -> transitPoints.addAll(place.getTransitPoints()));
        return transitPoints;
    }
    /**
     *  Delete the  transitPoint by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete TransitPoint : {}", id);
        transitPointRepository.delete(id);
        transitPointSearchRepository.delete(id);
    }

    /**
     * Search for the transitPoint corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<TransitPointDTO> search(String query) {
        log.debug("Request to search TransitPoints for query {}", query);
        return StreamSupport
            .stream(transitPointSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(transitPointMapper::transitPointToTransitPointDTO)
            .collect(Collectors.toList());
    }
}
