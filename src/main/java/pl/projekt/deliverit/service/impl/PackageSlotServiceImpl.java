package pl.projekt.deliverit.service.impl;

import pl.projekt.deliverit.service.PackageSlotService;
import pl.projekt.deliverit.domain.PackageSlot;
import pl.projekt.deliverit.repository.PackageSlotRepository;
import pl.projekt.deliverit.repository.search.PackageSlotSearchRepository;
import pl.projekt.deliverit.web.rest.dto.PackageSlotDTO;
import pl.projekt.deliverit.web.rest.mapper.PackageSlotMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing PackageSlot.
 */
@Service
@Transactional
public class PackageSlotServiceImpl implements PackageSlotService{

    private final Logger log = LoggerFactory.getLogger(PackageSlotServiceImpl.class);
    
    @Inject
    private PackageSlotRepository packageSlotRepository;
    
    @Inject
    private PackageSlotMapper packageSlotMapper;
    
    @Inject
    private PackageSlotSearchRepository packageSlotSearchRepository;
    
    /**
     * Save a packageSlot.
     * 
     * @param packageSlotDTO the entity to save
     * @return the persisted entity
     */
    public PackageSlotDTO save(PackageSlotDTO packageSlotDTO) {
        log.debug("Request to save PackageSlot : {}", packageSlotDTO);
        PackageSlot packageSlot = packageSlotMapper.packageSlotDTOToPackageSlot(packageSlotDTO);
        packageSlot = packageSlotRepository.save(packageSlot);
        PackageSlotDTO result = packageSlotMapper.packageSlotToPackageSlotDTO(packageSlot);
        packageSlotSearchRepository.save(packageSlot);
        return result;
    }

    /**
     *  Get all the packageSlots.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<PackageSlotDTO> findAll() {
        log.debug("Request to get all PackageSlots");
        List<PackageSlotDTO> result = packageSlotRepository.findAll().stream()
            .map(packageSlotMapper::packageSlotToPackageSlotDTO)
            .collect(Collectors.toCollection(LinkedList::new));
        return result;
    }

    /**
     *  Get one packageSlot by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public PackageSlotDTO findOne(Long id) {
        log.debug("Request to get PackageSlot : {}", id);
        PackageSlot packageSlot = packageSlotRepository.findOne(id);
        PackageSlotDTO packageSlotDTO = packageSlotMapper.packageSlotToPackageSlotDTO(packageSlot);
        return packageSlotDTO;
    }

    /**
     *  Delete the  packageSlot by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PackageSlot : {}", id);
        packageSlotRepository.delete(id);
        packageSlotSearchRepository.delete(id);
    }

    /**
     * Search for the packageSlot corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<PackageSlotDTO> search(String query) {
        log.debug("Request to search PackageSlots for query {}", query);
        return StreamSupport
            .stream(packageSlotSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(packageSlotMapper::packageSlotToPackageSlotDTO)
            .collect(Collectors.toList());
    }
}
