package pl.projekt.deliverit.service.impl;

import pl.projekt.deliverit.domain.ShipmentOffer;
import pl.projekt.deliverit.domain.ShipmentOfferSubmition;
import pl.projekt.deliverit.repository.ShipmentOfferRepository;
import pl.projekt.deliverit.repository.ShipmentOfferSubmitionRepository;
import pl.projekt.deliverit.service.PackService;
import pl.projekt.deliverit.domain.Pack;
import pl.projekt.deliverit.repository.PackRepository;
import pl.projekt.deliverit.repository.search.PackSearchRepository;
import pl.projekt.deliverit.web.rest.ShipmentOfferSubmitionResource;
import pl.projekt.deliverit.web.rest.dto.PackDTO;
import pl.projekt.deliverit.web.rest.mapper.PackMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Pack.
 */
@Service
@Transactional
public class PackServiceImpl implements PackService{

    private final Logger log = LoggerFactory.getLogger(PackServiceImpl.class);

    @Inject
    private PackRepository packRepository;

    @Inject
    private PackMapper packMapper;

    @Inject
    private ShipmentOfferRepository shipmentOfferRepository;

    @Inject
    private ShipmentOfferSubmitionRepository shipmentOfferSubmitionRepository;

    @Inject
    private PackSearchRepository packSearchRepository;

    /**
     * Save a pack.
     *
     * @param packDTO the entity to save
     * @return the persisted entity
     */
    public PackDTO save(PackDTO packDTO) {
        log.debug("Request to save Pack : {}", packDTO);
        Pack pack = packMapper.packDTOToPack(packDTO);
        pack = packRepository.save(pack);
        PackDTO result = packMapper.packToPackDTO(pack);
        packSearchRepository.save(pack);
        return result;
    }

    /**
     *  Get all the packs.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Pack> findAll(Pageable pageable) {
        log.debug("Request to get all Packs");
        Page<Pack> result = packRepository.findAll(pageable);
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PackDTO> findAllForDeliver() {
        log.debug("Request to get all Packs for Deliver");
        List<PackDTO> packDTOs = new ArrayList<>();
        List<ShipmentOffer> shipmentOffers = shipmentOfferRepository.findByUser_shipmentIsCurrentUser();
        shipmentOffers.stream().forEach(shipmentOffer -> shipmentOffer.getPackageSlots().stream().
            forEach(packageSlot -> packDTOs.
                addAll(packMapper.packsToPackDTOs(packRepository.findAllByPackageSlot(packageSlot))))
        );
        return packDTOs;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PackDTO> findAllForReceiver() {
        log.debug("Request to get all Packs for Receiver");
        List<PackDTO> packDTOs = new ArrayList<>();
        List<ShipmentOfferSubmition> shipmentOffers = shipmentOfferSubmitionRepository.findByUserIsCurrentUser();
        shipmentOffers.stream().forEach(shipmentOffer -> shipmentOffer.getPackagess().stream().
            forEach(pack -> packDTOs.add(packMapper.packToPackDTO(pack))));
        return packDTOs;
    }

    /**
     *  Get one pack by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PackDTO findOne(Long id) {
        log.debug("Request to get Pack : {}", id);
        Pack pack = packRepository.findOne(id);
        PackDTO packDTO = packMapper.packToPackDTO(pack);
        return packDTO;
    }

    /**
     *  Delete the  pack by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Pack : {}", id);
        packRepository.delete(id);
        packSearchRepository.delete(id);
    }

    /**
     * Search for the pack corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Pack> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Packs for query {}", query);
        return packSearchRepository.search(queryStringQuery(query), pageable);
    }
}
