package pl.projekt.deliverit.service;

import pl.projekt.deliverit.domain.PackageSlot;
import pl.projekt.deliverit.web.rest.dto.PackageSlotDTO;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing PackageSlot.
 */
public interface PackageSlotService {

    /**
     * Save a packageSlot.
     * 
     * @param packageSlotDTO the entity to save
     * @return the persisted entity
     */
    PackageSlotDTO save(PackageSlotDTO packageSlotDTO);

    /**
     *  Get all the packageSlots.
     *  
     *  @return the list of entities
     */
    List<PackageSlotDTO> findAll();

    /**
     *  Get the "id" packageSlot.
     *  
     *  @param id the id of the entity
     *  @return the entity
     */
    PackageSlotDTO findOne(Long id);

    /**
     *  Delete the "id" packageSlot.
     *  
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the packageSlot corresponding to the query.
     * 
     *  @param query the query of the search
     *  @return the list of entities
     */
    List<PackageSlotDTO> search(String query);
}
