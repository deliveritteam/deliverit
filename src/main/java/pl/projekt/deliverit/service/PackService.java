package pl.projekt.deliverit.service;

import pl.projekt.deliverit.domain.Pack;
import pl.projekt.deliverit.web.rest.dto.PackDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Pack.
 */
public interface PackService {

    /**
     * Save a pack.
     *
     * @param packDTO the entity to save
     * @return the persisted entity
     */
    PackDTO save(PackDTO packDTO);

    /**
     *  Get all the packs.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Pack> findAll(Pageable pageable);

    /**
     *  Get all the packs.
     *
     *  @return the list of entities
     */
    List<PackDTO> findAllForDeliver();

    /**
     *  Get all the packs.
     *
     *  @return the list of entities
     */
    List<PackDTO> findAllForReceiver();


    /**
     *  Get the "id" pack.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PackDTO findOne(Long id);

    /**
     *  Delete the "id" pack.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the pack corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    Page<Pack> search(String query, Pageable pageable);
}
