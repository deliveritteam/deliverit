package pl.projekt.deliverit.service;

import org.springframework.transaction.annotation.Transactional;
import pl.projekt.deliverit.domain.ShipmentOffer;
import pl.projekt.deliverit.web.rest.dto.ShipmentOfferDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing ShipmentOffer.
 */
public interface ShipmentOfferService {

    /**
     * Save a shipmentOffer.
     *
     * @param shipmentOfferDTO the entity to save
     * @return the persisted entity
     */
    ShipmentOfferDTO save(ShipmentOfferDTO shipmentOfferDTO);

    /**
     *  Get all the shipmentOffers.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ShipmentOffer> findAll(Pageable pageable);

    List<ShipmentOfferDTO> findAllWithCities();

    @Transactional(readOnly = true)
    ShipmentOfferDTO findOneWithCity(Long id);

    @Transactional(readOnly = true)
    List<ShipmentOfferDTO> findAllForCities(String fromCity, String toCity);

    /**
     *  Get the "id" shipmentOffer.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ShipmentOfferDTO findOne(Long id);

    @Transactional(readOnly = true)
    List<ShipmentOfferDTO> findAllForUser();

    /**
     *  Delete the "id" shipmentOffer.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the shipmentOffer corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    Page<ShipmentOffer> search(String query, Pageable pageable);
}
