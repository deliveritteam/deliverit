package pl.projekt.deliverit.service;

import org.springframework.transaction.annotation.Transactional;
import pl.projekt.deliverit.domain.TransitPoint;
import pl.projekt.deliverit.web.rest.dto.TransitPointDTO;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing TransitPoint.
 */
public interface TransitPointService {

    /**
     * Save a transitPoint.
     *
     * @param transitPointDTO the entity to save
     * @return the persisted entity
     */
    TransitPointDTO save(TransitPointDTO transitPointDTO);

    /**
     *  Get all the transitPoints.
     *
     *  @return the list of entities
     */
    List<TransitPointDTO> findAll();

    /**
     *  Get the "id" transitPoint.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    TransitPointDTO findOne(Long id);

    @Transactional(readOnly = true)
    List<TransitPoint> findByCity(String city);

    /**
     *  Delete the "id" transitPoint.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the transitPoint corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    List<TransitPointDTO> search(String query);
}
