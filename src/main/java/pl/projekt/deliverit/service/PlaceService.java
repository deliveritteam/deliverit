package pl.projekt.deliverit.service;

import pl.projekt.deliverit.domain.Place;
import pl.projekt.deliverit.domain.enumeration.PlaceStatus;
import pl.projekt.deliverit.web.rest.dto.PlaceDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Place.
 */
public interface PlaceService {

    /**
     * Save a place.
     *
     * @param placeDTO the entity to save
     * @return the persisted entity
     */
    PlaceDTO save(PlaceDTO placeDTO);

    /**
     *  Get all the places.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Place> findAll(Pageable pageable);

    /**
     *  Get all the places.
     *
     *
     *  @param placeStatus the placeStatus
     *  @return the list of entities
     */
    List<PlaceDTO> findAllByStatus(PlaceStatus placeStatus);
    /**
     *  Get the "id" place.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PlaceDTO findOne(Long id);

    /**
     *  Delete the "id" place.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the place corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    Page<Place> search(String query, Pageable pageable);
}
