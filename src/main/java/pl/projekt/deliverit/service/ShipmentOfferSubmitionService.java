package pl.projekt.deliverit.service;

import org.springframework.transaction.annotation.Transactional;
import pl.projekt.deliverit.domain.ShipmentOfferSubmition;
import pl.projekt.deliverit.web.rest.dto.ShipmentOfferSubmitionDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing ShipmentOfferSubmition.
 */
public interface ShipmentOfferSubmitionService {

    /**
     * Save a shipmentOfferSubmition.
     *
     * @param shipmentOfferSubmitionDTO the entity to save
     * @return the persisted entity
     */
    ShipmentOfferSubmitionDTO save(ShipmentOfferSubmitionDTO shipmentOfferSubmitionDTO);

    /**
     *  Get all the shipmentOfferSubmitions.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ShipmentOfferSubmition> findAll(Pageable pageable);

    /**
     *  Get the "id" shipmentOfferSubmition.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ShipmentOfferSubmitionDTO findOne(Long id);

    /**
     *  Delete the "id" shipmentOfferSubmition.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the shipmentOfferSubmition corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    Page<ShipmentOfferSubmition> search(String query, Pageable pageable);

    @Transactional
    List<ShipmentOfferSubmitionDTO> findAllForCurrentUser();

    @Transactional
    List<ShipmentOfferSubmitionDTO> findAllForCarrier();
}
