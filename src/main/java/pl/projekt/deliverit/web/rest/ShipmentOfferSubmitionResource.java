package pl.projekt.deliverit.web.rest;

import com.codahale.metrics.annotation.Timed;
import pl.projekt.deliverit.domain.ShipmentOfferSubmition;
import pl.projekt.deliverit.domain.enumeration.Status;
import pl.projekt.deliverit.service.ShipmentOfferSubmitionService;
import pl.projekt.deliverit.web.rest.dto.ShipmentOfferDTO;
import pl.projekt.deliverit.web.rest.util.HeaderUtil;
import pl.projekt.deliverit.web.rest.util.PaginationUtil;
import pl.projekt.deliverit.web.rest.dto.ShipmentOfferSubmitionDTO;
import pl.projekt.deliverit.web.rest.mapper.ShipmentOfferSubmitionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ShipmentOfferSubmition.
 */
@RestController
@RequestMapping("/api")
public class ShipmentOfferSubmitionResource {

    private final Logger log = LoggerFactory.getLogger(ShipmentOfferSubmitionResource.class);

    @Inject
    private ShipmentOfferSubmitionService shipmentOfferSubmitionService;

    @Inject
    private ShipmentOfferSubmitionMapper shipmentOfferSubmitionMapper;

    /**
     * POST  /shipment-offer-submitions : Create a new shipmentOfferSubmition.
     *
     * @param shipmentOfferSubmitionDTO the shipmentOfferSubmitionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new shipmentOfferSubmitionDTO, or with status 400 (Bad Request) if the shipmentOfferSubmition has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/shipment-offer-submitions",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ShipmentOfferSubmitionDTO> createShipmentOfferSubmition(@Valid @RequestBody ShipmentOfferSubmitionDTO shipmentOfferSubmitionDTO) throws URISyntaxException {
        log.debug("REST request to save ShipmentOfferSubmition : {}", shipmentOfferSubmitionDTO);
        if (shipmentOfferSubmitionDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("shipmentOfferSubmition", "idexists", "A new shipmentOfferSubmition cannot already have an ID")).body(null);
        }
        shipmentOfferSubmitionDTO.setDate(LocalDate.now());
        shipmentOfferSubmitionDTO.setStatus(Status.messageSent);
        ShipmentOfferSubmitionDTO result = shipmentOfferSubmitionService.save(shipmentOfferSubmitionDTO);
        return ResponseEntity.created(new URI("/api/shipment-offer-submitions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("shipmentOfferSubmition", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /shipment-offer-submitions : Updates an existing shipmentOfferSubmition.
     *
     * @param shipmentOfferSubmitionDTO the shipmentOfferSubmitionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated shipmentOfferSubmitionDTO,
     * or with status 400 (Bad Request) if the shipmentOfferSubmitionDTO is not valid,
     * or with status 500 (Internal Server Error) if the shipmentOfferSubmitionDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/shipment-offer-submitions",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ShipmentOfferSubmitionDTO> updateShipmentOfferSubmition(@Valid @RequestBody ShipmentOfferSubmitionDTO shipmentOfferSubmitionDTO) throws URISyntaxException {
        log.debug("REST request to update ShipmentOfferSubmition : {}", shipmentOfferSubmitionDTO);
        if (shipmentOfferSubmitionDTO.getId() == null) {
            return createShipmentOfferSubmition(shipmentOfferSubmitionDTO);
        }
        ShipmentOfferSubmitionDTO result = shipmentOfferSubmitionService.save(shipmentOfferSubmitionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("shipmentOfferSubmition", shipmentOfferSubmitionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /shipment-offer-submitions : get all the shipmentOfferSubmitions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of shipmentOfferSubmitions in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/shipment-offer-submitions",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ShipmentOfferSubmitionDTO>> getAllShipmentOfferSubmitions(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of ShipmentOfferSubmitions");
        Page<ShipmentOfferSubmition> page = shipmentOfferSubmitionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shipment-offer-submitions");
        return new ResponseEntity<>(shipmentOfferSubmitionMapper.shipmentOfferSubmitionsToShipmentOfferSubmitionDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/shipment-offer-submitions-user",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ShipmentOfferSubmitionDTO>> getAllShipmentOfferSubmitionsForUser()
        throws URISyntaxException {
        log.debug("REST request to get a page of ShipmentOfferSubmitions for User");
        List<ShipmentOfferSubmitionDTO> page = shipmentOfferSubmitionService.findAllForCurrentUser();
        return Optional.ofNullable(page)
            .map(result -> new ResponseEntity<>(
                page,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(value = "/shipment-offer-submitions-carrier",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ShipmentOfferSubmitionDTO>> getAllShipmentOfferSubmitionsForCarrier()
        throws URISyntaxException {
        log.debug("REST request to get a page of ShipmentOfferSubmitions for User");
        List<ShipmentOfferSubmitionDTO> page = shipmentOfferSubmitionService.findAllForCarrier();
        return Optional.ofNullable(page)
            .map(result -> new ResponseEntity<>(
                page,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    /**
     * GET  /shipment-offer-submitions/:id : get the "id" shipmentOfferSubmition.
     *
     * @param id the id of the shipmentOfferSubmitionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the shipmentOfferSubmitionDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/shipment-offer-submitions/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ShipmentOfferSubmitionDTO> getShipmentOfferSubmition(@PathVariable Long id) {
        log.debug("REST request to get ShipmentOfferSubmition : {}", id);
        ShipmentOfferSubmitionDTO shipmentOfferSubmitionDTO = shipmentOfferSubmitionService.findOne(id);
        return Optional.ofNullable(shipmentOfferSubmitionDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /shipment-offer-submitions/:id : delete the "id" shipmentOfferSubmition.
     *
     * @param id the id of the shipmentOfferSubmitionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/shipment-offer-submitions/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteShipmentOfferSubmition(@PathVariable Long id) {
        log.debug("REST request to delete ShipmentOfferSubmition : {}", id);
        shipmentOfferSubmitionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("shipmentOfferSubmition", id.toString())).build();
    }

    /**
     * SEARCH  /_search/shipment-offer-submitions?query=:query : search for the shipmentOfferSubmition corresponding
     * to the query.
     *
     * @param query the query of the shipmentOfferSubmition search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/shipment-offer-submitions",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ShipmentOfferSubmitionDTO>> searchShipmentOfferSubmitions(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of ShipmentOfferSubmitions for query {}", query);
        Page<ShipmentOfferSubmition> page = shipmentOfferSubmitionService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/shipment-offer-submitions");
        return new ResponseEntity<>(shipmentOfferSubmitionMapper.shipmentOfferSubmitionsToShipmentOfferSubmitionDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
