package pl.projekt.deliverit.web.rest;

import com.codahale.metrics.annotation.Timed;
import pl.projekt.deliverit.domain.ShipmentOffer;
import pl.projekt.deliverit.service.ShipmentOfferService;
import pl.projekt.deliverit.web.rest.util.HeaderUtil;
import pl.projekt.deliverit.web.rest.util.PaginationUtil;
import pl.projekt.deliverit.web.rest.dto.ShipmentOfferDTO;
import pl.projekt.deliverit.web.rest.mapper.ShipmentOfferMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ShipmentOffer.
 */
@RestController
@RequestMapping("/api")
public class ShipmentOfferResource {

    private final Logger log = LoggerFactory.getLogger(ShipmentOfferResource.class);

    @Inject
    private ShipmentOfferService shipmentOfferService;

    @Inject
    private ShipmentOfferMapper shipmentOfferMapper;

    /**
     * POST  /shipment-offers : Create a new shipmentOffer.
     *
     * @param shipmentOfferDTO the shipmentOfferDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new shipmentOfferDTO, or with status 400 (Bad Request) if the shipmentOffer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/shipment-offers",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ShipmentOfferDTO> createShipmentOffer(@RequestBody ShipmentOfferDTO shipmentOfferDTO) throws URISyntaxException {
        log.debug("REST request to save ShipmentOffer : {}", shipmentOfferDTO);
        if (shipmentOfferDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("shipmentOffer", "idexists", "A new shipmentOffer cannot already have an ID")).body(null);
        }
        shipmentOfferDTO.setDate(LocalDate.now());
        ShipmentOfferDTO result = shipmentOfferService.save(shipmentOfferDTO);
        return ResponseEntity.created(new URI("/api/shipment-offers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("shipmentOffer", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /shipment-offers : Updates an existing shipmentOffer.
     *
     * @param shipmentOfferDTO the shipmentOfferDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated shipmentOfferDTO,
     * or with status 400 (Bad Request) if the shipmentOfferDTO is not valid,
     * or with status 500 (Internal Server Error) if the shipmentOfferDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/shipment-offers",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ShipmentOfferDTO> updateShipmentOffer(@RequestBody ShipmentOfferDTO shipmentOfferDTO) throws URISyntaxException {
        log.debug("REST request to update ShipmentOffer : {}", shipmentOfferDTO);
        if (shipmentOfferDTO.getId() == null) {
            return createShipmentOffer(shipmentOfferDTO);
        }
        ShipmentOfferDTO result = shipmentOfferService.save(shipmentOfferDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("shipmentOffer", shipmentOfferDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /shipment-offers : get all the shipmentOffers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of shipmentOffers in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */

    @RequestMapping(value = "/shipment-offers",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ShipmentOfferDTO>> getAllShipmentOffers(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of ShipmentOffers");
        Page<ShipmentOffer> page = shipmentOfferService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shipment-offers");
        return new ResponseEntity<>(shipmentOfferMapper.shipmentOffersToShipmentOfferDTOs(page.getContent()), headers, HttpStatus.OK);
    }



    @RequestMapping(value = "/shipment-offers-cities",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ShipmentOfferDTO>> getAllShipmentOffersWithCities()
        throws URISyntaxException {
        log.debug("REST request to get a page of ShipmentOffers");
        List<ShipmentOfferDTO> page = shipmentOfferService.findAllWithCities();
        return Optional.ofNullable(page)
            .map(result -> new ResponseEntity<>(
                page,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(value = "/shipment-offers-user",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ShipmentOfferDTO>> getAllShipmentOffersForUser()
        throws URISyntaxException {
        log.debug("REST request to get a page of ShipmentOffers for current User");
        List<ShipmentOfferDTO> page = shipmentOfferService.findAllForUser();
        return Optional.ofNullable(page)
            .map(result -> new ResponseEntity<>(
                page,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /find-shipment-offers-cities/{fromCity}/{toCity} : get the "fromCity" and "toCity" shipmentOffer.
     *
     * @param fromCity from Which City Find
     * @param toCity to Which City Find
     * @return the ResponseEntity with status 200 (OK) and with body the shipmentOfferDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/find-shipment-offers-cities/{fromCity}/{toCity}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ShipmentOfferDTO>> getAllShipmentOffersForCities(@PathVariable String fromCity , @PathVariable String toCity)
        throws URISyntaxException {
        log.debug("REST find ShipmentOffers for cities");
        List<ShipmentOfferDTO> page = shipmentOfferService.findAllForCities(fromCity, toCity);
        return Optional.ofNullable(page)
            .map(result -> new ResponseEntity<>(
                page,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    /**
     * GET  /shipment-offers-cities/:id : get the "id" shipmentOffer .
     *
     * @param id the id of the shipmentOfferDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the shipmentOfferDTO (complete) , or with status 404 (Not Found)
     */
    @RequestMapping(value = "/shipment-offers-cities/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ShipmentOfferDTO> getShipmentOfferWithcCities(@PathVariable Long id) {
        log.debug("REST request to get ShipmentOffer : {}", id);
        ShipmentOfferDTO shipmentOfferDTO = shipmentOfferService.findOneWithCity(id);
        return Optional.ofNullable(shipmentOfferDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    /**
     * GET  /shipment-offers/:id : get the "id" shipmentOffer.
     *
     * @param id the id of the shipmentOfferDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the shipmentOfferDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/shipment-offers/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ShipmentOfferDTO> getShipmentOffer(@PathVariable Long id) {
        log.debug("REST request to get ShipmentOffer : {}", id);
        ShipmentOfferDTO shipmentOfferDTO = shipmentOfferService.findOne(id);
        return Optional.ofNullable(shipmentOfferDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /shipment-offers/:id : delete the "id" shipmentOffer.
     *
     * @param id the id of the shipmentOfferDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/shipment-offers/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteShipmentOffer(@PathVariable Long id) {
        log.debug("REST request to delete ShipmentOffer : {}", id);
        shipmentOfferService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("shipmentOffer", id.toString())).build();
    }

    /**
     * SEARCH  /_search/shipment-offers?query=:query : search for the shipmentOffer corresponding
     * to the query.
     *
     * @param query the query of the shipmentOffer search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/shipment-offers",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ShipmentOfferDTO>> searchShipmentOffers(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of ShipmentOffers for query {}", query);
        Page<ShipmentOffer> page = shipmentOfferService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/shipment-offers");
        return new ResponseEntity<>(shipmentOfferMapper.shipmentOffersToShipmentOfferDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
