package pl.projekt.deliverit.web.rest;

import com.codahale.metrics.annotation.Timed;
import pl.projekt.deliverit.domain.Pack;
import pl.projekt.deliverit.service.PackService;
import pl.projekt.deliverit.web.rest.util.HeaderUtil;
import pl.projekt.deliverit.web.rest.util.PaginationUtil;
import pl.projekt.deliverit.web.rest.dto.PackDTO;
import pl.projekt.deliverit.web.rest.mapper.PackMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Pack.
 */
@RestController
@RequestMapping("/api")
public class PackResource {

    private final Logger log = LoggerFactory.getLogger(PackResource.class);

    @Inject
    private PackService packService;

    @Inject
    private PackMapper packMapper;

    /**
     * POST  /packs : Create a new pack.
     *
     * @param packDTO the packDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new packDTO, or with status 400 (Bad Request) if the pack has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/packs",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PackDTO> createPack(@RequestBody PackDTO packDTO) throws URISyntaxException {
        log.debug("REST request to save Pack : {}", packDTO);
        if (packDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("pack", "idexists", "A new pack cannot already have an ID")).body(null);
        }
        PackDTO result = packService.save(packDTO);
        return ResponseEntity.created(new URI("/api/packs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("pack", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /packs : Updates an existing pack.
     *
     * @param packDTO the packDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated packDTO,
     * or with status 400 (Bad Request) if the packDTO is not valid,
     * or with status 500 (Internal Server Error) if the packDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/packs",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PackDTO> updatePack(@RequestBody PackDTO packDTO) throws URISyntaxException {
        log.debug("REST request to update Pack : {}", packDTO);
        if (packDTO.getId() == null) {
            return createPack(packDTO);
        }
        PackDTO result = packService.save(packDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("pack", packDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /packs : get all the packs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of packs in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/packs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<PackDTO>> getAllPacks(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Packs");
        Page<Pack> page = packService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/packs");
        return new ResponseEntity<>(packMapper.packsToPackDTOs(page.getContent()), headers, HttpStatus.OK);
    }


    /**
     * GET  /packs : get all the packs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of packs in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/deliver-packs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<PackDTO>> getAllDeliverPacks()
        throws URISyntaxException {
        log.debug("REST request to get a page of Packs");
        List<PackDTO> page = packService.findAllForDeliver();
        return Optional.ofNullable(page)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    @RequestMapping(value = "/receiver-packs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<PackDTO>> getAllReceiverPacks()
        throws URISyntaxException {
        log.debug("REST request to get a page of Packs");
        List<PackDTO> page = packService.findAllForReceiver();
        return Optional.ofNullable(page)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /packs/:id : get the "id" pack.
     *
     * @param id the id of the packDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the packDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/packs/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PackDTO> getPack(@PathVariable Long id) {
        log.debug("REST request to get Pack : {}", id);
        PackDTO packDTO = packService.findOne(id);
        return Optional.ofNullable(packDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /packs/:id : delete the "id" pack.
     *
     * @param id the id of the packDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/packs/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePack(@PathVariable Long id) {
        log.debug("REST request to delete Pack : {}", id);
        packService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("pack", id.toString())).build();
    }

    /**
     * SEARCH  /_search/packs?query=:query : search for the pack corresponding
     * to the query.
     *
     * @param query the query of the pack search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/packs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<PackDTO>> searchPacks(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Packs for query {}", query);
        Page<Pack> page = packService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/packs");
        return new ResponseEntity<>(packMapper.packsToPackDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
