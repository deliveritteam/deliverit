package pl.projekt.deliverit.web.rest.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Objects;

import pl.projekt.deliverit.domain.enumeration.Status;

/**
 * A DTO for the ShipmentOfferSubmition entity.
 */
public class ShipmentOfferSubmitionDTO implements Serializable {

    private Long id;

    @NotNull
    private Status status;


    @NotNull
    @Size(max = 264)
    private String message;


    private LocalDate date;

    private List<PackDTO> packagesDTOs;

    @NotNull
    private LocalDate receipDate;


    private Long startTransitPointId;
    private Long userId;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
    public LocalDate getReceipDate() {
        return receipDate;
    }

    public void setReceipDate(LocalDate receipDate) {
        this.receipDate = receipDate;
    }

    public Long getStartTransitPointId() {
        return startTransitPointId;
    }

    public void setStartTransitPointId(Long transitPointId) {
        this.startTransitPointId = transitPointId;
    }
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShipmentOfferSubmitionDTO shipmentOfferSubmitionDTO = (ShipmentOfferSubmitionDTO) o;

        if ( ! Objects.equals(id, shipmentOfferSubmitionDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ShipmentOfferSubmitionDTO{" +
            "id=" + id +
            ", status='" + status + "'" +
            ", message='" + message + "'" +
            ", date='" + date + "'" +
            ", receipDate='" + receipDate + "'" +
            '}';
    }

    public List<PackDTO> getPackagesDTOs() {
        return packagesDTOs;
    }

    public void setPackagesDTOs(List<PackDTO> packagesDTOs) {
        this.packagesDTOs = packagesDTOs;
    }
}
