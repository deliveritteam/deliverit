package pl.projekt.deliverit.web.rest.mapper;

import pl.projekt.deliverit.domain.*;
import pl.projekt.deliverit.web.rest.dto.PlaceDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Place and its DTO PlaceDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, })
public interface PlaceMapper {

    @Mapping(source = "user.id", target = "userId")
    PlaceDTO placeToPlaceDTO(Place place);

    List<PlaceDTO> placesToPlaceDTOs(List<Place> places);

    @Mapping(source = "userId", target = "user")
    @Mapping(target = "transitPoints", ignore = true)
    Place placeDTOToPlace(PlaceDTO placeDTO);

    List<Place> placeDTOsToPlaces(List<PlaceDTO> placeDTOs);
}
