/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package pl.projekt.deliverit.web.rest.dto;
