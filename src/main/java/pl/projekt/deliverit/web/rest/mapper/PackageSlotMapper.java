package pl.projekt.deliverit.web.rest.mapper;

import pl.projekt.deliverit.domain.*;
import pl.projekt.deliverit.web.rest.dto.PackageSlotDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity PackageSlot and its DTO PackageSlotDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PackageSlotMapper {

    @Mapping(source = "shipmentOffer.id", target = "shipmentOfferId")
    PackageSlotDTO packageSlotToPackageSlotDTO(PackageSlot packageSlot);

    List<PackageSlotDTO> packageSlotsToPackageSlotDTOs(List<PackageSlot> packageSlots);

    @Mapping(source = "shipmentOfferId", target = "shipmentOffer")
    PackageSlot packageSlotDTOToPackageSlot(PackageSlotDTO packageSlotDTO);

    List<PackageSlot> packageSlotDTOsToPackageSlots(List<PackageSlotDTO> packageSlotDTOs);

    default ShipmentOffer shipmentOfferFromId(Long id) {
        if (id == null) {
            return null;
        }
        ShipmentOffer shipmentOffer = new ShipmentOffer();
        shipmentOffer.setId(id);
        return shipmentOffer;
    }
}
