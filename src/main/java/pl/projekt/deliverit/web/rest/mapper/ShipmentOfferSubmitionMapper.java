package pl.projekt.deliverit.web.rest.mapper;

import pl.projekt.deliverit.domain.*;
import pl.projekt.deliverit.web.rest.dto.ShipmentOfferSubmitionDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity ShipmentOfferSubmition and its DTO ShipmentOfferSubmitionDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, })
public interface ShipmentOfferSubmitionMapper {

    @Mapping(source = "startTransitPoint.id", target = "startTransitPointId")
    @Mapping(source = "user.id", target = "userId")
    ShipmentOfferSubmitionDTO shipmentOfferSubmitionToShipmentOfferSubmitionDTO(ShipmentOfferSubmition shipmentOfferSubmition);

    List<ShipmentOfferSubmitionDTO> shipmentOfferSubmitionsToShipmentOfferSubmitionDTOs(List<ShipmentOfferSubmition> shipmentOfferSubmitions);

    @Mapping(source = "startTransitPointId", target = "startTransitPoint")
    @Mapping(source = "userId", target = "user")
    @Mapping(target = "packagess", ignore = true)
    ShipmentOfferSubmition shipmentOfferSubmitionDTOToShipmentOfferSubmition(ShipmentOfferSubmitionDTO shipmentOfferSubmitionDTO);

    List<ShipmentOfferSubmition> shipmentOfferSubmitionDTOsToShipmentOfferSubmitions(List<ShipmentOfferSubmitionDTO> shipmentOfferSubmitionDTOs);

    default TransitPoint transitPointFromId(Long id) {
        if (id == null) {
            return null;
        }
        TransitPoint transitPoint = new TransitPoint();
        transitPoint.setId(id);
        return transitPoint;
    }
}
