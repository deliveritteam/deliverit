package pl.projekt.deliverit.web.rest.mapper;

import pl.projekt.deliverit.domain.*;
import pl.projekt.deliverit.web.rest.dto.TransitPointDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity TransitPoint and its DTO TransitPointDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TransitPointMapper {

    @Mapping(source = "shipmentOffer.id", target = "shipmentOfferId")
    @Mapping(source = "place.id", target = "placeId")
    TransitPointDTO transitPointToTransitPointDTO(TransitPoint transitPoint);

    List<TransitPointDTO> transitPointsToTransitPointDTOs(List<TransitPoint> transitPoints);

    @Mapping(source = "shipmentOfferId", target = "shipmentOffer")
    @Mapping(source = "placeId", target = "place")
    TransitPoint transitPointDTOToTransitPoint(TransitPointDTO transitPointDTO);

    List<TransitPoint> transitPointDTOsToTransitPoints(List<TransitPointDTO> transitPointDTOs);

    default ShipmentOffer shipmentOfferFromId(Long id) {
        if (id == null) {
            return null;
        }
        ShipmentOffer shipmentOffer = new ShipmentOffer();
        shipmentOffer.setId(id);
        return shipmentOffer;
    }

    default Place placeFromId(Long id) {
        if (id == null) {
            return null;
        }
        Place place = new Place();
        place.setId(id);
        return place;
    }
}
