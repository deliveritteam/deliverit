package pl.projekt.deliverit.web.rest.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the PackageSlot entity.
 */
public class PackageSlotDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 30)
    private String size;


    private Float price;


    private Integer quantity;


    @NotNull
    private Integer remainingQuantity;


    private Long shipmentOfferId;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    public Integer getRemainingQuantity() {
        return remainingQuantity;
    }

    public void setRemainingQuantity(Integer remainingQuantity) {
        this.remainingQuantity = remainingQuantity;
    }

    public Long getShipmentOfferId() {
        return shipmentOfferId;
    }

    public void setShipmentOfferId(Long shipmentOfferId) {
        this.shipmentOfferId = shipmentOfferId;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PackageSlotDTO packageSlotDTO = (PackageSlotDTO) o;

        if ( ! Objects.equals(id, packageSlotDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PackageSlotDTO{" +
            "id=" + id +
            ", size='" + size + "'" +
            ", price='" + price + "'" +
            ", quantity='" + quantity + "'" +
            ", remainingQuantity='" + remainingQuantity + "'" +
            '}';
    }
}
