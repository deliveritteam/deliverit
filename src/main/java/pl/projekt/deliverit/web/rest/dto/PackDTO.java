package pl.projekt.deliverit.web.rest.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the Pack entity.
 */
public class PackDTO implements Serializable {

    private Long id;

    private String packageCode;


    private Long packageSlotId;
    private Long endTrasitPointId;
    private Long shipmentOfferSubmitionId;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    public Long getPackageSlotId() {
        return packageSlotId;
    }

    public void setPackageSlotId(Long packageSlotId) {
        this.packageSlotId = packageSlotId;
    }
    public Long getEndTrasitPointId() {
        return endTrasitPointId;
    }

    public void setEndTrasitPointId(Long transitPointId) {
        this.endTrasitPointId = transitPointId;
    }
    public Long getShipmentOfferSubmitionId() {
        return shipmentOfferSubmitionId;
    }

    public void setShipmentOfferSubmitionId(Long shipmentOfferSubmitionId) {
        this.shipmentOfferSubmitionId = shipmentOfferSubmitionId;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PackDTO packDTO = (PackDTO) o;

        if ( ! Objects.equals(id, packDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PackDTO{" +
            "id=" + id +
            ", packageCode='" + packageCode + "'" +
            '}';
    }
}
