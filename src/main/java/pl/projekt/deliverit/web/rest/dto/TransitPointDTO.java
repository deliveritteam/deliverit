package pl.projekt.deliverit.web.rest.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the TransitPoint entity.
 */
public class TransitPointDTO implements Serializable {

    private Long id;

    private String city;


    private ZonedDateTime date;


    private Long shipmentOfferId;
    private Long placeId;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public Long getShipmentOfferId() {
        return shipmentOfferId;
    }

    public void setShipmentOfferId(Long shipmentOfferId) {
        this.shipmentOfferId = shipmentOfferId;
    }
    public Long getPlaceId() {
        return placeId;
    }

    public void setPlaceId(Long placeId) {
        this.placeId = placeId;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TransitPointDTO transitPointDTO = (TransitPointDTO) o;

        if ( ! Objects.equals(id, transitPointDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TransitPointDTO{" +
            "id=" + id +
            ", city='" + city + "'" +
            ", date='" + date + "'" +
            '}';
    }
}
