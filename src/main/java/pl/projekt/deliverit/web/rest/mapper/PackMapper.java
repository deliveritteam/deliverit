package pl.projekt.deliverit.web.rest.mapper;

import pl.projekt.deliverit.domain.*;
import pl.projekt.deliverit.web.rest.dto.PackDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Pack and its DTO PackDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PackMapper {

    @Mapping(source = "packageSlot.id", target = "packageSlotId")
    @Mapping(source = "endTrasitPoint.id", target = "endTrasitPointId")
    @Mapping(source = "shipmentOfferSubmition.id", target = "shipmentOfferSubmitionId")
    PackDTO packToPackDTO(Pack pack);

    List<PackDTO> packsToPackDTOs(List<Pack> packs);

    @Mapping(source = "packageSlotId", target = "packageSlot")
    @Mapping(source = "endTrasitPointId", target = "endTrasitPoint")
    @Mapping(source = "shipmentOfferSubmitionId", target = "shipmentOfferSubmition")
    Pack packDTOToPack(PackDTO packDTO);

    List<Pack> packDTOsToPacks(List<PackDTO> packDTOs);

    default PackageSlot packageSlotFromId(Long id) {
        if (id == null) {
            return null;
        }
        PackageSlot packageSlot = new PackageSlot();
        packageSlot.setId(id);
        return packageSlot;
    }

    default TransitPoint transitPointFromId(Long id) {
        if (id == null) {
            return null;
        }
        TransitPoint transitPoint = new TransitPoint();
        transitPoint.setId(id);
        return transitPoint;
    }

    default ShipmentOfferSubmition shipmentOfferSubmitionFromId(Long id) {
        if (id == null) {
            return null;
        }
        ShipmentOfferSubmition shipmentOfferSubmition = new ShipmentOfferSubmition();
        shipmentOfferSubmition.setId(id);
        return shipmentOfferSubmition;
    }
}
