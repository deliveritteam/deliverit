package pl.projekt.deliverit.web.rest.mapper;

import pl.projekt.deliverit.domain.*;
import pl.projekt.deliverit.web.rest.dto.ShipmentOfferDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity ShipmentOffer and its DTO ShipmentOfferDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, })
public interface ShipmentOfferMapper {

    @Mapping(source = "user_shipment.id", target = "user_shipmentId")
    @Mapping(target = "packageSlots", ignore = true)
    ShipmentOfferDTO shipmentOfferToShipmentOfferDTO(ShipmentOffer shipmentOffer);

    List<ShipmentOfferDTO> shipmentOffersToShipmentOfferDTOs(List<ShipmentOffer> shipmentOffers);

    @Mapping(source = "user_shipmentId", target = "user_shipment")
    @Mapping(target = "transitPoints", ignore = true)
    @Mapping(target = "packageSlots", ignore = true)
    ShipmentOffer shipmentOfferDTOToShipmentOffer(ShipmentOfferDTO shipmentOfferDTO);

    List<ShipmentOffer> shipmentOfferDTOsToShipmentOffers(List<ShipmentOfferDTO> shipmentOfferDTOs);
}
