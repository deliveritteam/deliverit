package pl.projekt.deliverit.web.rest.dto;

import pl.projekt.deliverit.domain.PackageSlot;
import pl.projekt.deliverit.domain.TransitPoint;

import java.time.LocalDate;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the ShipmentOffer entity.
 */
public class ShipmentOfferDTO implements Serializable {

    private Long id;

    private LocalDate date;


    private String description;

    private List<PackageSlotDTO> packageSlots;

    private Long user_shipmentId;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
    public String getDescription() {
        return description;
    }

    public List<TransitPointDTO> transitPoints;



    public void setDescription(String description) {
        this.description = description;
    }

    public Long getUser_shipmentId() {
        return user_shipmentId;
    }

    public void setUser_shipmentId(Long userId) {
        this.user_shipmentId = userId;
    }

    public List<PackageSlotDTO> getPackageSlots() {
        return packageSlots;
    }

    public void setPackageSlots(List<PackageSlotDTO> packageSlots) {
        this.packageSlots = packageSlots;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShipmentOfferDTO shipmentOfferDTO = (ShipmentOfferDTO) o;

        if ( ! Objects.equals(id, shipmentOfferDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ShipmentOfferDTO{" +
            "id=" + id +
            ", date='" + date + "'" +
            ", description='" + description + "'" +
            '}';
    }
}
