package pl.projekt.deliverit.web.rest;

import com.codahale.metrics.annotation.Timed;
import pl.projekt.deliverit.domain.PackageSlot;
import pl.projekt.deliverit.service.PackageSlotService;
import pl.projekt.deliverit.web.rest.util.HeaderUtil;
import pl.projekt.deliverit.web.rest.dto.PackageSlotDTO;
import pl.projekt.deliverit.web.rest.mapper.PackageSlotMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PackageSlot.
 */
@RestController
@RequestMapping("/api")
public class PackageSlotResource {

    private final Logger log = LoggerFactory.getLogger(PackageSlotResource.class);
        
    @Inject
    private PackageSlotService packageSlotService;
    
    @Inject
    private PackageSlotMapper packageSlotMapper;
    
    /**
     * POST  /package-slots : Create a new packageSlot.
     *
     * @param packageSlotDTO the packageSlotDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new packageSlotDTO, or with status 400 (Bad Request) if the packageSlot has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/package-slots",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PackageSlotDTO> createPackageSlot(@Valid @RequestBody PackageSlotDTO packageSlotDTO) throws URISyntaxException {
        log.debug("REST request to save PackageSlot : {}", packageSlotDTO);
        if (packageSlotDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("packageSlot", "idexists", "A new packageSlot cannot already have an ID")).body(null);
        }
        PackageSlotDTO result = packageSlotService.save(packageSlotDTO);
        return ResponseEntity.created(new URI("/api/package-slots/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("packageSlot", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /package-slots : Updates an existing packageSlot.
     *
     * @param packageSlotDTO the packageSlotDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated packageSlotDTO,
     * or with status 400 (Bad Request) if the packageSlotDTO is not valid,
     * or with status 500 (Internal Server Error) if the packageSlotDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/package-slots",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PackageSlotDTO> updatePackageSlot(@Valid @RequestBody PackageSlotDTO packageSlotDTO) throws URISyntaxException {
        log.debug("REST request to update PackageSlot : {}", packageSlotDTO);
        if (packageSlotDTO.getId() == null) {
            return createPackageSlot(packageSlotDTO);
        }
        PackageSlotDTO result = packageSlotService.save(packageSlotDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("packageSlot", packageSlotDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /package-slots : get all the packageSlots.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of packageSlots in body
     */
    @RequestMapping(value = "/package-slots",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public List<PackageSlotDTO> getAllPackageSlots() {
        log.debug("REST request to get all PackageSlots");
        return packageSlotService.findAll();
    }

    /**
     * GET  /package-slots/:id : get the "id" packageSlot.
     *
     * @param id the id of the packageSlotDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the packageSlotDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/package-slots/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PackageSlotDTO> getPackageSlot(@PathVariable Long id) {
        log.debug("REST request to get PackageSlot : {}", id);
        PackageSlotDTO packageSlotDTO = packageSlotService.findOne(id);
        return Optional.ofNullable(packageSlotDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /package-slots/:id : delete the "id" packageSlot.
     *
     * @param id the id of the packageSlotDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/package-slots/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePackageSlot(@PathVariable Long id) {
        log.debug("REST request to delete PackageSlot : {}", id);
        packageSlotService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("packageSlot", id.toString())).build();
    }

    /**
     * SEARCH  /_search/package-slots?query=:query : search for the packageSlot corresponding
     * to the query.
     *
     * @param query the query of the packageSlot search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/package-slots",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public List<PackageSlotDTO> searchPackageSlots(@RequestParam String query) {
        log.debug("REST request to search PackageSlots for query {}", query);
        return packageSlotService.search(query);
    }

}
