package pl.projekt.deliverit.web.rest;

import com.codahale.metrics.annotation.Timed;
import pl.projekt.deliverit.domain.TransitPoint;
import pl.projekt.deliverit.service.TransitPointService;
import pl.projekt.deliverit.web.rest.util.HeaderUtil;
import pl.projekt.deliverit.web.rest.dto.TransitPointDTO;
import pl.projekt.deliverit.web.rest.mapper.TransitPointMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TransitPoint.
 */
@RestController
@RequestMapping("/api")
public class TransitPointResource {

    private final Logger log = LoggerFactory.getLogger(TransitPointResource.class);

    @Inject
    private TransitPointService transitPointService;

    @Inject
    private TransitPointMapper transitPointMapper;

    /**
     * POST  /transit-points : Create a new transitPoint.
     *
     * @param transitPointDTO the transitPointDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new transitPointDTO, or with status 400 (Bad Request) if the transitPoint has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/transit-points",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TransitPointDTO> createTransitPoint(@RequestBody TransitPointDTO transitPointDTO) throws URISyntaxException {
        log.debug("REST request to save TransitPoint : {}", transitPointDTO);
        if (transitPointDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("transitPoint", "idexists", "A new transitPoint cannot already have an ID")).body(null);
        }
        TransitPointDTO result = transitPointService.save(transitPointDTO);
        return ResponseEntity.created(new URI("/api/transit-points/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("transitPoint", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /transit-points : Updates an existing transitPoint.
     *
     * @param transitPointDTO the transitPointDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated transitPointDTO,
     * or with status 400 (Bad Request) if the transitPointDTO is not valid,
     * or with status 500 (Internal Server Error) if the transitPointDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/transit-points",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TransitPointDTO> updateTransitPoint(@RequestBody TransitPointDTO transitPointDTO) throws URISyntaxException {
        log.debug("REST request to update TransitPoint : {}", transitPointDTO);
        if (transitPointDTO.getId() == null) {
            return createTransitPoint(transitPointDTO);
        }
        TransitPointDTO result = transitPointService.save(transitPointDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("transitPoint", transitPointDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /transit-points : get all the transitPoints.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of transitPoints in body
     */
    @RequestMapping(value = "/transit-points",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public List<TransitPointDTO> getAllTransitPoints() {
        log.debug("REST request to get all TransitPoints");
        return transitPointService.findAll();
    }

    /**
     * GET  /transit-points/:id : get the "id" transitPoint.
     *
     * @param id the id of the transitPointDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the transitPointDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/transit-points/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TransitPointDTO> getTransitPoint(@PathVariable Long id) {
        log.debug("REST request to get TransitPoint : {}", id);
        TransitPointDTO transitPointDTO = transitPointService.findOne(id);
        return Optional.ofNullable(transitPointDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /transit-points/:id : delete the "id" transitPoint.
     *
     * @param id the id of the transitPointDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/transit-points/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTransitPoint(@PathVariable Long id) {
        log.debug("REST request to delete TransitPoint : {}", id);
        transitPointService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("transitPoint", id.toString())).build();
    }

    /**
     * SEARCH  /_search/transit-points?query=:query : search for the transitPoint corresponding
     * to the query.
     *
     * @param query the query of the transitPoint search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/transit-points",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public List<TransitPointDTO> searchTransitPoints(@RequestParam String query) {
        log.debug("REST request to search TransitPoints for query {}", query);
        return transitPointService.search(query);
    }

}
