package pl.projekt.deliverit.repository;

import pl.projekt.deliverit.domain.PackageSlot;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the PackageSlot entity.
 */
public interface PackageSlotRepository extends JpaRepository<PackageSlot,Long> {

}
