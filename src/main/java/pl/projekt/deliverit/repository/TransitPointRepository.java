package pl.projekt.deliverit.repository;

import pl.projekt.deliverit.domain.TransitPoint;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TransitPoint entity.
 */
public interface TransitPointRepository extends JpaRepository<TransitPoint,Long> {
    List<TransitPoint> findAllByCity(String city);
}
