package pl.projekt.deliverit.repository;

import pl.projekt.deliverit.domain.ShipmentOfferSubmition;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ShipmentOfferSubmition entity.
 */
public interface ShipmentOfferSubmitionRepository extends JpaRepository<ShipmentOfferSubmition,Long> {

    @Query("select shipmentOfferSubmition from ShipmentOfferSubmition shipmentOfferSubmition where shipmentOfferSubmition.user.login = ?#{principal.username}")
    List<ShipmentOfferSubmition> findByUserIsCurrentUser();

}
