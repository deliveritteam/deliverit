package pl.projekt.deliverit.repository;

import pl.projekt.deliverit.domain.Place;

import org.springframework.data.jpa.repository.*;
import pl.projekt.deliverit.domain.enumeration.PlaceStatus;

import java.util.List;

/**
 * Spring Data JPA repository for the Place entity.
 */
public interface PlaceRepository extends JpaRepository<Place,Long> {

    @Query("select place from Place place where place.user.login = ?#{principal.username}")
    List<Place> findByUserIsCurrentUser();

    List<Place> findByCityContains(String city);
    List<Place> findByCityContainingIgnoreCase(String city);
    List<Place> findByStatus(PlaceStatus status);

}
