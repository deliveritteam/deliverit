package pl.projekt.deliverit.repository;

import pl.projekt.deliverit.domain.ShipmentOffer;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ShipmentOffer entity.
 */
public interface ShipmentOfferRepository extends JpaRepository<ShipmentOffer,Long> {

    @Query("select shipmentOffer from ShipmentOffer shipmentOffer where shipmentOffer.user_shipment.login = ?#{principal.username}")
    List<ShipmentOffer> findByUser_shipmentIsCurrentUser();


}
