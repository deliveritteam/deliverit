package pl.projekt.deliverit.repository.search;

import pl.projekt.deliverit.domain.ShipmentOffer;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ShipmentOffer entity.
 */
public interface ShipmentOfferSearchRepository extends ElasticsearchRepository<ShipmentOffer, Long> {
}
