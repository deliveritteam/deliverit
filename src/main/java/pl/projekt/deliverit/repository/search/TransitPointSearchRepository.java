package pl.projekt.deliverit.repository.search;

import pl.projekt.deliverit.domain.TransitPoint;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the TransitPoint entity.
 */
public interface TransitPointSearchRepository extends ElasticsearchRepository<TransitPoint, Long> {
}
