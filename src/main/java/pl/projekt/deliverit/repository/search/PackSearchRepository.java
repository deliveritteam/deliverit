package pl.projekt.deliverit.repository.search;

import pl.projekt.deliverit.domain.Pack;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Pack entity.
 */
public interface PackSearchRepository extends ElasticsearchRepository<Pack, Long> {
}
