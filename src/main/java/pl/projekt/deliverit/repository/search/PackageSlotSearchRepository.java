package pl.projekt.deliverit.repository.search;

import pl.projekt.deliverit.domain.PackageSlot;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the PackageSlot entity.
 */
public interface PackageSlotSearchRepository extends ElasticsearchRepository<PackageSlot, Long> {
}
