package pl.projekt.deliverit.repository.search;

import pl.projekt.deliverit.domain.ShipmentOfferSubmition;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ShipmentOfferSubmition entity.
 */
public interface ShipmentOfferSubmitionSearchRepository extends ElasticsearchRepository<ShipmentOfferSubmition, Long> {
}
