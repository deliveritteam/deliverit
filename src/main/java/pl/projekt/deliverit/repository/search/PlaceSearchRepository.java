package pl.projekt.deliverit.repository.search;

import pl.projekt.deliverit.domain.Place;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import pl.projekt.deliverit.domain.enumeration.PlaceStatus;

import java.util.List;

/**
 * Spring Data ElasticSearch repository for the Place entity.
 */
public interface PlaceSearchRepository extends ElasticsearchRepository<Place, Long> {
    List<Place> findByCityLike(String city);
}
