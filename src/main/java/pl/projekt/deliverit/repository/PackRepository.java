package pl.projekt.deliverit.repository;

import pl.projekt.deliverit.domain.Pack;

import org.springframework.data.jpa.repository.*;
import pl.projekt.deliverit.domain.PackageSlot;

import java.util.List;

/**
 * Spring Data JPA repository for the Pack entity.
 */
public interface PackRepository extends JpaRepository<Pack,Long> {
    public List<Pack> findAllByPackageSlot(PackageSlot packageSlot);
}
