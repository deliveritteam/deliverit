(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('ShipmentOfferSubmitionDeleteController',ShipmentOfferSubmitionDeleteController);

    ShipmentOfferSubmitionDeleteController.$inject = ['$uibModalInstance', 'entity', 'ShipmentOfferSubmition'];

    function ShipmentOfferSubmitionDeleteController($uibModalInstance, entity, ShipmentOfferSubmition) {
        var vm = this;
        vm.shipmentOfferSubmition = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            ShipmentOfferSubmition.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
