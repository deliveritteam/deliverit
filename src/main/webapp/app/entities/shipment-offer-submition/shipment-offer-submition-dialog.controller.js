(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('ShipmentOfferSubmitionDialogController', ShipmentOfferSubmitionDialogController);

    ShipmentOfferSubmitionDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'ShipmentOfferSubmition', 'TransitPoint', 'User', 'Pack'];

    function ShipmentOfferSubmitionDialogController ($scope, $stateParams, $uibModalInstance, entity, ShipmentOfferSubmition, TransitPoint, User, Pack) {
        var vm = this;
        vm.shipmentOfferSubmition = entity;
        vm.transitpoints = TransitPoint.query();
        vm.users = User.query();
        vm.packs = Pack.query();
        vm.load = function(id) {
            ShipmentOfferSubmition.get({id : id}, function(result) {
                vm.shipmentOfferSubmition = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('deliveritApp:shipmentOfferSubmitionUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.shipmentOfferSubmition.id !== null) {
                ShipmentOfferSubmition.update(vm.shipmentOfferSubmition, onSaveSuccess, onSaveError);
            } else {
                ShipmentOfferSubmition.save(vm.shipmentOfferSubmition, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.date = false;
        vm.datePickerOpenStatus.receipDate = false;

        vm.openCalendar = function(date) {
            vm.datePickerOpenStatus[date] = true;
        };
    }
})();
