(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('ShipmentOfferSubmitionDetailController', ShipmentOfferSubmitionDetailController);

    ShipmentOfferSubmitionDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'ShipmentOfferSubmition', 'TransitPoint', 'User', 'Pack'];

    function ShipmentOfferSubmitionDetailController($scope, $rootScope, $stateParams, entity, ShipmentOfferSubmition, TransitPoint, User, Pack) {
        var vm = this;
        vm.shipmentOfferSubmition = entity;
        vm.load = function (id) {
            ShipmentOfferSubmition.get({id: id}, function(result) {
                vm.shipmentOfferSubmition = result;
            });
        };
        var unsubscribe = $rootScope.$on('deliveritApp:shipmentOfferSubmitionUpdate', function(event, result) {
            vm.shipmentOfferSubmition = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
