(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .factory('ShipmentOfferSubmitionSearch', ShipmentOfferSubmitionSearch);

    ShipmentOfferSubmitionSearch.$inject = ['$resource'];

    function ShipmentOfferSubmitionSearch($resource) {
        var resourceUrl =  'api/_search/shipment-offer-submitions/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
