(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('shipment-offer-submition', {
            parent: 'entity',
            url: '/shipment-offer-submition?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'ShipmentOfferSubmitions'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/shipment-offer-submition/shipment-offer-submitions.html',
                    controller: 'ShipmentOfferSubmitionController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('shipment-offer-submition-detail', {
            parent: 'entity',
            url: '/shipment-offer-submition/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'ShipmentOfferSubmition'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/shipment-offer-submition/shipment-offer-submition-detail.html',
                    controller: 'ShipmentOfferSubmitionDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'ShipmentOfferSubmition', function($stateParams, ShipmentOfferSubmition) {
                    return ShipmentOfferSubmition.get({id : $stateParams.id});
                }]
            }
        })
        .state('shipment-offer-submition.new', {
            parent: 'shipment-offer-submition',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/shipment-offer-submition/shipment-offer-submition-dialog.html',
                    controller: 'ShipmentOfferSubmitionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                status: null,
                                message: null,
                                date: null,
                                receipDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('shipment-offer-submition', null, { reload: true });
                }, function() {
                    $state.go('shipment-offer-submition');
                });
            }]
        })
        .state('shipment-offer-submition.edit', {
            parent: 'shipment-offer-submition',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/shipment-offer-submition/shipment-offer-submition-dialog.html',
                    controller: 'ShipmentOfferSubmitionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ShipmentOfferSubmition', function(ShipmentOfferSubmition) {
                            return ShipmentOfferSubmition.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('shipment-offer-submition', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('shipment-offer-submition.delete', {
            parent: 'shipment-offer-submition',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/shipment-offer-submition/shipment-offer-submition-delete-dialog.html',
                    controller: 'ShipmentOfferSubmitionDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ShipmentOfferSubmition', function(ShipmentOfferSubmition) {
                            return ShipmentOfferSubmition.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('shipment-offer-submition', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
