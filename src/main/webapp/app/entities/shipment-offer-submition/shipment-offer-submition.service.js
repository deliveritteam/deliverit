(function() {
    'use strict';
    angular
        .module('deliveritApp')
        .factory('ShipmentOfferSubmition', ShipmentOfferSubmition);

    ShipmentOfferSubmition.$inject = ['$resource', 'DateUtils'];

    function ShipmentOfferSubmition ($resource, DateUtils) {
        var resourceUrl =  'api/shipment-offer-submitions/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.date = DateUtils.convertLocalDateFromServer(data.date);
                    data.receipDate = DateUtils.convertLocalDateFromServer(data.receipDate);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.date = DateUtils.convertLocalDateToServer(data.date);
                    data.receipDate = DateUtils.convertLocalDateToServer(data.receipDate);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.date = DateUtils.convertLocalDateToServer(data.date);
                    data.receipDate = DateUtils.convertLocalDateToServer(data.receipDate);
                    return angular.toJson(data);
                }
            }
        });
    }
})();
