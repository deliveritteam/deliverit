(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('PackDetailController', PackDetailController);

    PackDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Pack', 'PackageSlot', 'TransitPoint', 'ShipmentOfferSubmition'];

    function PackDetailController($scope, $rootScope, $stateParams, entity, Pack, PackageSlot, TransitPoint, ShipmentOfferSubmition) {
        var vm = this;
        vm.pack = entity;
        vm.load = function (id) {
            Pack.get({id: id}, function(result) {
                vm.pack = result;
            });
        };
        var unsubscribe = $rootScope.$on('deliveritApp:packUpdate', function(event, result) {
            vm.pack = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
