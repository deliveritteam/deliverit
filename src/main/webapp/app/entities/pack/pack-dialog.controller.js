(function () {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('PackDialogController', PackDialogController);

    PackDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Pack', 'PackageSlot', 'TransitPoint', 'ShipmentOfferSubmition'];

    function PackDialogController ($scope, $stateParams, $uibModalInstance, entity, Pack, PackageSlot, TransitPoint, ShipmentOfferSubmition) {
        var vm = this;
        vm.pack = entity;
        vm.packageslots = PackageSlot.query();
        vm.transitpoints = TransitPoint.query();
        vm.shipmentoffersubmitions = ShipmentOfferSubmition.query();
        vm.load = function(id) {
            Pack.get({id : id}, function(result) {
                vm.pack = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('deliveritApp:packUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.pack.id !== null) {
                Pack.update(vm.pack, onSaveSuccess, onSaveError);
            } else {
                Pack.save(vm.pack, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
