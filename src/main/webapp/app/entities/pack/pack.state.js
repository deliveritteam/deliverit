(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('pack', {
            parent: 'entity',
            url: '/pack?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Packs'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/pack/packs.html',
                    controller: 'PackController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('pack-detail', {
            parent: 'entity',
            url: '/pack/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Pack'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/pack/pack-detail.html',
                    controller: 'PackDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Pack', function($stateParams, Pack) {
                    return Pack.get({id : $stateParams.id});
                }]
            }
        })
        .state('pack.new', {
            parent: 'pack',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/pack/pack-dialog.html',
                    controller: 'PackDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                packageCode: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('pack', null, { reload: true });
                }, function() {
                    $state.go('pack');
                });
            }]
        })
        .state('pack.edit', {
            parent: 'pack',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/pack/pack-dialog.html',
                    controller: 'PackDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Pack', function(Pack) {
                            return Pack.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('pack', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('pack.delete', {
            parent: 'pack',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/pack/pack-delete-dialog.html',
                    controller: 'PackDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Pack', function(Pack) {
                            return Pack.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('pack', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
