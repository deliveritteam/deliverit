(function() {
    'use strict';
    angular
        .module('deliveritApp')
        .factory('Pack', Pack);

    Pack.$inject = ['$resource'];

    function Pack ($resource) {
        var resourceUrl =  'api/packs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
