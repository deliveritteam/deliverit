(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('PackDeleteController',PackDeleteController);

    PackDeleteController.$inject = ['$uibModalInstance', 'entity', 'Pack'];

    function PackDeleteController($uibModalInstance, entity, Pack) {
        var vm = this;
        vm.pack = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            Pack.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
