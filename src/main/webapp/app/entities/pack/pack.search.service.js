(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .factory('PackSearch', PackSearch);

    PackSearch.$inject = ['$resource'];

    function PackSearch($resource) {
        var resourceUrl =  'api/_search/packs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
