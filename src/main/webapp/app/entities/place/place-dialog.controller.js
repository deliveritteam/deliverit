(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('PlaceDialogController', PlaceDialogController);

    PlaceDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Place', 'User', 'TransitPoint'];

    function PlaceDialogController ($scope, $stateParams, $uibModalInstance, entity, Place, User, TransitPoint) {
        var vm = this;
        vm.place = entity;
        vm.users = User.query();
        vm.transitpoints = TransitPoint.query();
        vm.load = function(id) {
            Place.get({id : id}, function(result) {
                vm.place = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('deliveritApp:placeUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.place.id !== null) {
                Place.update(vm.place, onSaveSuccess, onSaveError);
            } else {
                Place.save(vm.place, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.creationDate = false;

        vm.openCalendar = function(date) {
            vm.datePickerOpenStatus[date] = true;
        };
    }
})();
