(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('PlaceDetailController', PlaceDetailController);

    PlaceDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Place', 'User', 'TransitPoint'];

    function PlaceDetailController($scope, $rootScope, $stateParams, entity, Place, User, TransitPoint) {
        var vm = this;
        vm.place = entity;
        vm.load = function (id) {
            Place.get({id: id}, function(result) {
                vm.place = result;
            });
        };
        var unsubscribe = $rootScope.$on('deliveritApp:placeUpdate', function(event, result) {
            vm.place = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
