(function() {
    'use strict';
    angular
        .module('deliveritApp')
        .factory('Place', Place);

    Place.$inject = ['$resource', 'DateUtils'];

    function Place ($resource, DateUtils) {
        var resourceUrl =  'api/places/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.creationDate = DateUtils.convertDateTimeFromServer(data.creationDate);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
