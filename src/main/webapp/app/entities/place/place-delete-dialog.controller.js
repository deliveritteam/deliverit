(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('PlaceDeleteController',PlaceDeleteController);

    PlaceDeleteController.$inject = ['$uibModalInstance', 'entity', 'Place'];

    function PlaceDeleteController($uibModalInstance, entity, Place) {
        var vm = this;
        vm.place = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            Place.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
