(function() {
    'use strict';
    angular
        .module('deliveritApp')
        .factory('PackageSlot', PackageSlot);

    PackageSlot.$inject = ['$resource'];

    function PackageSlot ($resource) {
        var resourceUrl =  'api/package-slots/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
