(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('PackageSlotController', PackageSlotController);

    PackageSlotController.$inject = ['$scope', '$state', 'PackageSlot', 'PackageSlotSearch'];

    function PackageSlotController ($scope, $state, PackageSlot, PackageSlotSearch) {
        var vm = this;
        vm.packageSlots = [];
        vm.loadAll = function() {
            PackageSlot.query(function(result) {
                vm.packageSlots = result;
            });
        };

        vm.search = function () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            PackageSlotSearch.query({query: vm.searchQuery}, function(result) {
                vm.packageSlots = result;
            });
        };
        vm.loadAll();
        
    }
})();
