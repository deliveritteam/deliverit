(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('PackageSlotDetailController', PackageSlotDetailController);

    PackageSlotDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'PackageSlot', 'ShipmentOffer'];

    function PackageSlotDetailController($scope, $rootScope, $stateParams, entity, PackageSlot, ShipmentOffer) {
        var vm = this;
        vm.packageSlot = entity;
        vm.load = function (id) {
            PackageSlot.get({id: id}, function(result) {
                vm.packageSlot = result;
            });
        };
        var unsubscribe = $rootScope.$on('deliveritApp:packageSlotUpdate', function(event, result) {
            vm.packageSlot = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
