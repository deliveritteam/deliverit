(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('package-slot', {
            parent: 'entity',
            url: '/package-slot',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'PackageSlots'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/package-slot/package-slots.html',
                    controller: 'PackageSlotController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('package-slot-detail', {
            parent: 'entity',
            url: '/package-slot/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'PackageSlot'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/package-slot/package-slot-detail.html',
                    controller: 'PackageSlotDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'PackageSlot', function($stateParams, PackageSlot) {
                    return PackageSlot.get({id : $stateParams.id});
                }]
            }
        })
        .state('package-slot.new', {
            parent: 'package-slot',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/package-slot/package-slot-dialog.html',
                    controller: 'PackageSlotDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                size: null,
                                price: null,
                                quantity: null,
                                remainingQuantity: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('package-slot', null, { reload: true });
                }, function() {
                    $state.go('package-slot');
                });
            }]
        })
        .state('package-slot.edit', {
            parent: 'package-slot',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/package-slot/package-slot-dialog.html',
                    controller: 'PackageSlotDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PackageSlot', function(PackageSlot) {
                            return PackageSlot.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('package-slot', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('package-slot.delete', {
            parent: 'package-slot',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/package-slot/package-slot-delete-dialog.html',
                    controller: 'PackageSlotDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['PackageSlot', function(PackageSlot) {
                            return PackageSlot.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('package-slot', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
