(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .factory('PackageSlotSearch', PackageSlotSearch);

    PackageSlotSearch.$inject = ['$resource'];

    function PackageSlotSearch($resource) {
        var resourceUrl =  'api/_search/package-slots/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
