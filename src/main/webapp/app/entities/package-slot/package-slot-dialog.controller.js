(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('PackageSlotDialogController', PackageSlotDialogController);

    PackageSlotDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'PackageSlot', 'ShipmentOffer'];

    function PackageSlotDialogController ($scope, $stateParams, $uibModalInstance, entity, PackageSlot, ShipmentOffer) {
        var vm = this;
        vm.packageSlot = entity;
        vm.shipmentoffers = ShipmentOffer.query();
        vm.load = function(id) {
            PackageSlot.get({id : id}, function(result) {
                vm.packageSlot = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('deliveritApp:packageSlotUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.packageSlot.id !== null) {
                PackageSlot.update(vm.packageSlot, onSaveSuccess, onSaveError);
            } else {
                PackageSlot.save(vm.packageSlot, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
