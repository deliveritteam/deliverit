(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('PackageSlotDeleteController',PackageSlotDeleteController);

    PackageSlotDeleteController.$inject = ['$uibModalInstance', 'entity', 'PackageSlot'];

    function PackageSlotDeleteController($uibModalInstance, entity, PackageSlot) {
        var vm = this;
        vm.packageSlot = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            PackageSlot.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
