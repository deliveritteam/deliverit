(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('ShipmentOfferDialogController', ShipmentOfferDialogController);

    ShipmentOfferDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'ShipmentOffer', 'User', 'TransitPoint', 'PackageSlot'];

    function ShipmentOfferDialogController ($scope, $stateParams, $uibModalInstance, entity, ShipmentOffer, User, TransitPoint, PackageSlot) {
        var vm = this;
        vm.shipmentOffer = entity;
        vm.users = User.query();
        vm.transitpoints = TransitPoint.query();
        vm.packageslots = PackageSlot.query();
        vm.load = function(id) {
            ShipmentOffer.get({id : id}, function(result) {
                vm.shipmentOffer = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('deliveritApp:shipmentOfferUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.shipmentOffer.id !== null) {
                ShipmentOffer.update(vm.shipmentOffer, onSaveSuccess, onSaveError);
            } else {
                ShipmentOffer.save(vm.shipmentOffer, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.date = false;

        vm.openCalendar = function(date) {
            vm.datePickerOpenStatus[date] = true;
        };
    }
})();
