(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .factory('ShipmentOfferSearch', ShipmentOfferSearch);

    ShipmentOfferSearch.$inject = ['$resource'];

    function ShipmentOfferSearch($resource) {
        var resourceUrl =  'api/_search/shipment-offers/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
