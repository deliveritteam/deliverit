(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('ShipmentOfferDeleteController',ShipmentOfferDeleteController);

    ShipmentOfferDeleteController.$inject = ['$uibModalInstance', 'entity', 'ShipmentOffer'];

    function ShipmentOfferDeleteController($uibModalInstance, entity, ShipmentOffer) {
        var vm = this;
        vm.shipmentOffer = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            ShipmentOffer.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
