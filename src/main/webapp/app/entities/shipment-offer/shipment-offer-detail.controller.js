(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('ShipmentOfferDetailController', ShipmentOfferDetailController);

    ShipmentOfferDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'ShipmentOffer', 'User', 'TransitPoint', 'PackageSlot'];

    function ShipmentOfferDetailController($scope, $rootScope, $stateParams, entity, ShipmentOffer, User, TransitPoint, PackageSlot) {
        var vm = this;
        vm.shipmentOffer = entity;
        vm.load = function (id) {
            ShipmentOffer.get({id: id}, function(result) {
                vm.shipmentOffer = result;
            });
        };
        var unsubscribe = $rootScope.$on('deliveritApp:shipmentOfferUpdate', function(event, result) {
            vm.shipmentOffer = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
