(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('shipment-offer', {
            parent: 'entity',
            url: '/shipment-offer?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'ShipmentOffers'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/shipment-offer/shipment-offers.html',
                    controller: 'ShipmentOfferController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('shipment-offer-detail', {
            parent: 'entity',
            url: '/shipment-offer/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'ShipmentOffer'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/shipment-offer/shipment-offer-detail.html',
                    controller: 'ShipmentOfferDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'ShipmentOffer', function($stateParams, ShipmentOffer) {
                    return ShipmentOffer.get({id : $stateParams.id});
                }]
            }
        })
        .state('shipment-offer.new', {
            parent: 'shipment-offer',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/shipment-offer/shipment-offer-dialog.html',
                    controller: 'ShipmentOfferDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                date: null,
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('shipment-offer', null, { reload: true });
                }, function() {
                    $state.go('shipment-offer');
                });
            }]
        })
        .state('shipment-offer.edit', {
            parent: 'shipment-offer',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/shipment-offer/shipment-offer-dialog.html',
                    controller: 'ShipmentOfferDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ShipmentOffer', function(ShipmentOffer) {
                            return ShipmentOffer.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('shipment-offer', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('shipment-offer.delete', {
            parent: 'shipment-offer',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/shipment-offer/shipment-offer-delete-dialog.html',
                    controller: 'ShipmentOfferDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ShipmentOffer', function(ShipmentOffer) {
                            return ShipmentOffer.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('shipment-offer', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
