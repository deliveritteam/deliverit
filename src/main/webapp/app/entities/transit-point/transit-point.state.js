(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('transit-point', {
            parent: 'entity',
            url: '/transit-point',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'TransitPoints'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/transit-point/transit-points.html',
                    controller: 'TransitPointController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('transit-point-detail', {
            parent: 'entity',
            url: '/transit-point/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'TransitPoint'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/transit-point/transit-point-detail.html',
                    controller: 'TransitPointDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'TransitPoint', function($stateParams, TransitPoint) {
                    return TransitPoint.get({id : $stateParams.id});
                }]
            }
        })
        .state('transit-point.new', {
            parent: 'transit-point',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/transit-point/transit-point-dialog.html',
                    controller: 'TransitPointDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                city: null,
                                date: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('transit-point', null, { reload: true });
                }, function() {
                    $state.go('transit-point');
                });
            }]
        })
        .state('transit-point.edit', {
            parent: 'transit-point',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/transit-point/transit-point-dialog.html',
                    controller: 'TransitPointDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TransitPoint', function(TransitPoint) {
                            return TransitPoint.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('transit-point', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('transit-point.delete', {
            parent: 'transit-point',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/transit-point/transit-point-delete-dialog.html',
                    controller: 'TransitPointDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TransitPoint', function(TransitPoint) {
                            return TransitPoint.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('transit-point', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
