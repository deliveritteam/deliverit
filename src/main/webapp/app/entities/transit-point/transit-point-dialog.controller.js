(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('TransitPointDialogController', TransitPointDialogController);

    TransitPointDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'TransitPoint', 'ShipmentOffer', 'Place'];

    function TransitPointDialogController ($scope, $stateParams, $uibModalInstance, entity, TransitPoint, ShipmentOffer, Place) {
        var vm = this;
        vm.transitPoint = entity;
        vm.shipmentoffers = ShipmentOffer.query();
        vm.places = Place.query();
        vm.load = function(id) {
            TransitPoint.get({id : id}, function(result) {
                vm.transitPoint = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('deliveritApp:transitPointUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.transitPoint.id !== null) {
                TransitPoint.update(vm.transitPoint, onSaveSuccess, onSaveError);
            } else {
                TransitPoint.save(vm.transitPoint, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.date = false;

        vm.openCalendar = function(date) {
            vm.datePickerOpenStatus[date] = true;
        };
    }
})();
