(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('TransitPointController', TransitPointController);

    TransitPointController.$inject = ['$scope', '$state', 'TransitPoint', 'TransitPointSearch'];

    function TransitPointController ($scope, $state, TransitPoint, TransitPointSearch) {
        var vm = this;
        vm.transitPoints = [];
        vm.loadAll = function() {
            TransitPoint.query(function(result) {
                vm.transitPoints = result;
            });
        };

        vm.search = function () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            TransitPointSearch.query({query: vm.searchQuery}, function(result) {
                vm.transitPoints = result;
            });
        };
        vm.loadAll();
        
    }
})();
