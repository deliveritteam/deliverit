(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .factory('TransitPointSearch', TransitPointSearch);

    TransitPointSearch.$inject = ['$resource'];

    function TransitPointSearch($resource) {
        var resourceUrl =  'api/_search/transit-points/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
