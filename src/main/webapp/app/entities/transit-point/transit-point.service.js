(function() {
    'use strict';
    angular
        .module('deliveritApp')
        .factory('TransitPoint', TransitPoint);

    TransitPoint.$inject = ['$resource', 'DateUtils'];

    function TransitPoint ($resource, DateUtils) {
        var resourceUrl =  'api/transit-points/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.date = DateUtils.convertDateTimeFromServer(data.date);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
