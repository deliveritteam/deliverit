(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('TransitPointDetailController', TransitPointDetailController);

    TransitPointDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'TransitPoint', 'ShipmentOffer', 'Place'];

    function TransitPointDetailController($scope, $rootScope, $stateParams, entity, TransitPoint, ShipmentOffer, Place) {
        var vm = this;
        vm.transitPoint = entity;
        vm.load = function (id) {
            TransitPoint.get({id: id}, function(result) {
                vm.transitPoint = result;
            });
        };
        var unsubscribe = $rootScope.$on('deliveritApp:transitPointUpdate', function(event, result) {
            vm.transitPoint = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
