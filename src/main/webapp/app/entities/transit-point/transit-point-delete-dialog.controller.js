(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('TransitPointDeleteController',TransitPointDeleteController);

    TransitPointDeleteController.$inject = ['$uibModalInstance', 'entity', 'TransitPoint'];

    function TransitPointDeleteController($uibModalInstance, entity, TransitPoint) {
        var vm = this;
        vm.transitPoint = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            TransitPoint.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
