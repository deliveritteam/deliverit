(function () {
    'use strict';
    angular
        .module('deliveritApp')
        .factory('SearchOfferCities', SearchOfferCities);

    SearchOfferCities.$inject = ['$resource'];

    function SearchOfferCities($resource) {
        var resourceUrl = 'api/shipment-offers-cities';

        return $resource(resourceUrl, {}, {
            'query': {method: 'GET', isArray: true}
        });
    }
})();
