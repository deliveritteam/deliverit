(function () {
    'use strict';

    angular
        .module('deliveritApp')
        .factory('SearchOfferCitiesSearch', SearchOfferCitiesSearch);

    SearchOfferCitiesSearch.$inject = ['$resource'];

    function SearchOfferCitiesSearch($resource) {
        var resourceUrl = 'api/find-shipment-offers-cities/:fromCity/:toCity';

        return $resource(resourceUrl, {}, {
            'query': {method: 'GET', isArray: true}
        });
    }
})();
