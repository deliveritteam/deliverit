(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('SearchOfferController', SearchOfferController);

    SearchOfferController.$inject = ['$scope', '$state', 'SearchOfferCities', 'SearchOfferCitiesSearch'];

    function SearchOfferController($scope, $state, SearchOfferCities, SearchOfferCitiesSearch) {
        var vm = this;
        vm.loadAll = loadAll;
        vm.search = search;
        vm.loadAll();

        function loadAll() {
            SearchOfferCities.query(function (result) {
                vm.shipmentOffersCities = result;
            });
        }

        function search(fromCity, toCity) {
            if (!vm.fromCity.length || !vm.toCity.length) {
                return vm.loadAll();
            }

            SearchOfferCitiesSearch.query({fromCity: fromCity, toCity: toCity}, function (result) {
                vm.shipmentOffersCities = result;
            });
        }
    }
})();
