(function () {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('AcceptOfferDialogController', AcceptOfferDialogController);

    AcceptOfferDialogController.$inject = ['Principal', '$q', '$rootScope', '$stateParams', '$uibModalInstance', 'entity', 'ShipmentOffer',
        'TransitPoint', 'PackageSlot', 'User', 'Pack', 'ShipmentOfferSubmition'];

    function AcceptOfferDialogController(Principal, $q, $rootScope, $stateParams, $uibModalInstance, entity, ShipmentOffer, TransitPoint,
                                         PackageSlot, User, Pack, ShipmentOfferSubmition) {
        var vm = this;

        vm.shipmentOfferSubmition = Object.assign({}, entity);
        vm.pack = Object.assign({}, entity);

        vm.offer = $rootScope.offerDetail;
        console.log("OFFERDETAIL: ");
        console.log(vm.offer);

        vm.tempId = undefined;
        vm.loadShipmentOffersSubmition();

        // temporary
        vm.loadShipmentOffersSubmition = function () {
            ShipmentOfferSubmition.query(function (result) {
                vm.tempId = result.length();
            });
        }

        var generatePackageCode = function () {
            var result = '';
            var codeLength = 6;
            var chars = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            for (var i = codeLength; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
            return result;
        };

        vm.createOfferSubmition = function () {
            vm.shipmentOfferSubmition.date = vm.offer.date;
            // vm.shipmentOfferSubmition.receipDate = vm.offer.transitPoints[vm.pack.endTrasitPointId].date;
            vm.shipmentOfferSubmition.receipDate = vm.offer.date;
            vm.shipmentOfferSubmition.status = "messageSent";
            vm.shipmentOfferSubmition.userId = vm.user.id;

            vm.pack.packageCode = generatePackageCode();

            ShipmentOfferSubmition.save(vm.shipmentOfferSubmition);
            vm.pack.shipmentOfferSubmitionId = vm.tempId + 1;
            Pack.save(vm.pack);

            $uibModalInstance.close();
        };

        var promiseAccount = getAccount();
        promiseAccount.then(function () {
            getUser()
        });

        function getAccount() {
            var deferred = $q.defer();
            Principal.identity().then(function (account) {
                vm.account = account;
                deferred.resolve(vm.account);
            });
            return deferred.promise;
        }

        function getUser() {
            var deferred = $q.defer();
            User.get({login: vm.account.login}, function (result) {
                vm.user = result;
                deferred.resolve(vm.user);
            });
            return deferred.promise;
        }

        vm.clear = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
