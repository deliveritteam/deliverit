(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('OfferDetailDialogController', OfferDetailDialogController);

    OfferDetailDialogController.$inject = ['$scope', '$q', '$rootScope', '$stateParams', 'SearchOfferCity'];

    function OfferDetailDialogController($scope, $q, $rootScope, $stateParams, SearchOfferCity) {
        var vm = this;
        loadCity();

        function loadCity() {
            var deferred = $q.defer();
            SearchOfferCity.get({id: $stateParams.id}, function (result) {
                vm.offerDetail = result;
                deferred.resolve(vm.offerDetail);
            });
            return deferred.promise;
        };
        var loadPromise = loadCity();
        loadPromise.then(function () {
            $rootScope.offerDetail = vm.offerDetail;
        });
    }
})();
