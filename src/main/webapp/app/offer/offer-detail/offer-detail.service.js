(function () {
    'use strict';
    angular
        .module('deliveritApp')
        .factory('SearchOfferCity', SearchOfferCity);

    SearchOfferCity.$inject = ['$resource'];

    function SearchOfferCity($resource) {
        var resourceUrl = 'api/shipment-offers-cities/:id';

        return $resource(resourceUrl, {}, {
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            }
        });
    }
})();
