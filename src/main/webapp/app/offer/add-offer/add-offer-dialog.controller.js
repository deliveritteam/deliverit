(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('AddOfferDialogController', AddOfferDialogController);

    AddOfferDialogController.$inject = ['Principal', '$q', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ShipmentOffer',
        'TransitPoint', 'PackageSlot', 'User'];

    function AddOfferDialogController(Principal, $q, $scope, $stateParams, $uibModalInstance, entity, ShipmentOffer, TransitPoint,
                                      PackageSlot, User) {
        var vm = this;
        vm.shipmentOffer = Object.assign({}, entity);
        vm.transitPointStart = Object.assign({}, entity);
        vm.transitPointEnd = Object.assign({}, entity);
        vm.packageSlot = Object.assign({}, entity);
        vm.transits = [];
        vm.slots = [{}];

        vm.shipmentOfferSubmition = Object.assign({}, entity);
        vm.pack = Object.assign({}, entity);

        vm.addTransit = function () {
            var tempId = vm.transits.length + 1;
            var tempDate = "vm.datePickerOpenStatus.field_transit_date_" + tempId;
            var tempFieldName = "field_transit_" + tempId;
            var tempFieldDateName = "field_transit_date_" + tempId;
            vm.transits.push({
                datePickerName: tempDate,
                fieldName: tempFieldName,
                fieldDateName: tempFieldDateName
            });
        }

        vm.addSlot = function () {
            var tempId = vm.slots.length + 1;
            var tempFieldSizeName = "field_slot_size_" + tempId;
            var tempFieldQuantityName = "field_slot_quantity_" + tempId;
            var tempFieldPriceName = "field_slot_price" + tempId;
            vm.slots.push({
                fieldSizeName: tempFieldSizeName,
                fieldQuantityName: tempFieldQuantityName,
                fieldPriceName: tempFieldPriceName
            });
        }

        /////////////////////////////////////////////////////////
        // ---------- WARNING! EYES BLEEDING CONTENT ----------//
        // ---------- * Reading for your own risk    ----------//
        /////////////////////////////////////////////////////////
        var promiseAccount = getAccount();
        promiseAccount.then(function () {
            getUser()
        });

        function getAccount() {
            var deferred = $q.defer();
            Principal.identity().then(function (account) {
                vm.account = account;
                deferred.resolve(vm.account);
            });
            return deferred.promise;
        }

        function getUser() {
            var deferred = $q.defer();
            User.get({login: vm.account.login}, function (result) {
                vm.user = result;
                deferred.resolve(vm.user);
            });
            return deferred.promise;
        }

        var currentDate = new Date();
        vm.shipmentOffer.date = currentDate;
        vm.resultCreateOffer = ShipmentOffer.save(vm.shipmentOffer);

        function createOffer() {
            var deferred = $q.defer();
            deferred.resolve(vm.resultCreateOffer);
            return deferred.promise;
        }

        function loadShipmentOffers() {
            var deferred = $q.defer();
            ShipmentOffer.query(function (result) {
                vm.shipmentOffers = result;
                deferred.resolve(vm.shipmentOffers);
            });
            return deferred.promise;
        }

        function createTransits() {
            var deferred = $q.defer();
            var tempId = vm.shipmentOffers.length - 1;
            vm.transitPointStart.shipmentOfferId = vm.shipmentOffers[tempId].id;
            vm.transitPointEnd.shipmentOfferId = vm.shipmentOffers[tempId].id;
            TransitPoint.save(vm.transitPointStart);

            for (var i = 0; i < vm.transits.length; i++) {
                vm.temp = Object.assign({}, entity);
                vm.temp.city = vm.transits[i].city;
                vm.temp.date = vm.transits[i].date;
                vm.temp.shipmentOfferId = vm.shipmentOffers[tempId].id;
                if (vm.temp.city != "" && vm.temp.date != "")
                    TransitPoint.save(vm.temp);
            }

            var result = TransitPoint.save(vm.transitPointEnd);
            deferred.resolve(result);
            return deferred.promise;
        }

        function createPackages() {
            var deferred = $q.defer();
            var tempId = vm.shipmentOffers.length - 1;

            for (var i = 0; i < vm.slots.length; i++) {
                vm.temp = Object.assign({}, entity);
                vm.temp.size = vm.slots[i].size;
                vm.temp.quantity = vm.slots[i].quantity;
                vm.temp.remainingQuantity = vm.temp.quantity;
                vm.temp.price = vm.slots[i].price;
                vm.temp.shipmentOfferId = vm.shipmentOffers[tempId].id;
                if (vm.temp.size != "" && vm.temp.quantity != "" && vm.temp.price != "")
                    PackageSlot.save(vm.temp);
            }

            // Just for resolve
            PackageSlot.query(function (result) {
                vm.temporary = result;
                deferred.resolve(vm.temporary);
            });
            return deferred.promise;
        }

        vm.save = function () {
            var promiseCreate = createOffer();
            promiseCreate.then(function () {
                var promiseLoad = loadShipmentOffers();
                promiseLoad.then(function () {
                    var promiseTransits = createTransits();
                    promiseTransits.then(function () {
                        var promisePackages = createPackages();
                        promisePackages.then(function () {
                            vm.shipmentOffer.user_shipmentId = vm.user.id;
                            var tempId = vm.shipmentOffers.length - 1;
                            vm.shipmentOffer.id = vm.shipmentOffers[tempId].id;
                            ShipmentOffer.update(vm.shipmentOffer);
                            $uibModalInstance.close();
                            vm.isSaving = false;
                        });
                    });
                });
            });
        }

        /////////////////////////////////////////////////////////////////////////////////////////////
        // ---------- You probably cant read this, sorry for that. Unlucky there is more  ---------//
        /////////////////////////////////////////////////////////////////////////////////////////////

        // *!* Zamienić to na fora */!*
        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.field_start_date = false;
        vm.datePickerOpenStatus.field_end_date = false;
        vm.datePickerOpenStatus.field_transit_date_1 = false;
        vm.datePickerOpenStatus.field_transit_date_2 = false;
        vm.datePickerOpenStatus.field_transit_date_3 = false;
        vm.datePickerOpenStatus.field_transit_date_4 = false;
        vm.datePickerOpenStatus.field_transit_date_5 = false;

        vm.openCalendar = function (date) {
            vm.datePickerOpenStatus[date] = true;
        };

        vm.clear = function() {
            var deletePromiseLoad = loadShipmentOffers();
            deletePromiseLoad.then(function () {
                var tempId = vm.shipmentOffers.length - 1;
                vm.shipmentOffer.id = vm.shipmentOffers[tempId].id;
                ShipmentOffer.delete(vm.shipmentOffer);
            });
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
