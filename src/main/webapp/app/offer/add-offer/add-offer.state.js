(function () {
    'use strict';

    angular
        .module('deliveritApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('add-offer', {
                abstract: true,
                parent: "offer"
            })
            .state('add-offer.new', {
                parent: 'add-offer',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/offer/add-offer/add-offer-dialog.html',
                        controller: 'AddOfferDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    packageCode: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function () {
                        $state.go('home', null, {reload: true});
                    }, function () {
                        $state.go('home');
                    });
                }]
            });
    }
})();
