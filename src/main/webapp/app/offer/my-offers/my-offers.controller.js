(function () {
    'use strict';

    angular
        .module('deliveritApp')
        .controller('MyOffersController', MyOffersController);

    MyOffersController.$inject = ['Principal', '$q', '$filter', '$scope', '$rootScope', '$stateParams', 'ShipmentOfferSubmition',
        'Pack', 'User', 'PackageSlot', 'TransitPoint', 'SearchOfferCities'];

    function MyOffersController(Principal, $q, $filter, $scope, $rootScope, $stateParams, ShipmentOfferSubmition, Pack,
                                User, PackageSlot, TransitPoint, SearchOfferCities) {
        var vm = this;
        vm.loadAll = loadAll;
        vm.loadAll();

        vm.tempId = undefined;

        function loadAll() {
            ShipmentOfferSubmition.query(function (result) {
                vm.shipmentOffersSubmition = result;
                console.log("ShipmentOfferSubmition");
                console.log(vm.shipmentOffersSubmition);
            });
            Pack.query(function (result) {
                vm.packs = result;
                console.log("Pack");
                console.log(vm.packs);
            });
            PackageSlot.query(function (result) {
                vm.packageSlots = result;
                console.log("PackageSlot");
                console.log(vm.packageSlots);
            });
            TransitPoint.query(function (result) {
                vm.transitPoints = result;
                console.log("TransitPoints");
                console.log(vm.transitPoints);
            });
            SearchOfferCities.query(function (result) {
                vm.shipmentOfferCities = result;
                console.log("SearchOfferCities");
                console.log(vm.shipmentOfferCities);
            });
        }


        var promiseAccount = getAccount();
        promiseAccount.then(function () {
            getUser()
        });

        function getAccount() {
            var deferred = $q.defer();
            Principal.identity().then(function (account) {
                vm.account = account;
                deferred.resolve(vm.account);
            });
            return deferred.promise;
        }

        function getUser() {
            var deferred = $q.defer();
            User.get({login: vm.account.login}, function (result) {
                vm.user = result;
                deferred.resolve(vm.user);
            });
            return deferred.promise;
        }

    }
})();
