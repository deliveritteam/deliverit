(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('my-offers', {
                parent: 'app',
                url: '/my-offers',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'My Offers'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/offer/my-offers/my-offers.html',
                        controller: 'MyOffersController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('accept-offer', {
                parent: 'app',
                url: '/accept-offer',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/offer/accept-offer/accept-offer-dialog.html',
                        controller: 'AcceptOfferDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                            }
                        }
                    }).result.then(function () {
                        $state.go('search-offer', null, {reload: true});
                    }, function () {
                        $state.go('search-offer');
                    });
                }]
            })
            .state('search-offer', {
                parent: 'app',
                url: '/search-offer-cities',
                data: {
                    pageTitle: 'Offer detail'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/offer/search-offer/search-offer.html',
                        controller: 'SearchOfferController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('offer-detail', {
                parent: 'app',
                url: '/offer-detail/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Offer detail'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/offer/offer-detail/offer-detail.html',
                        controller: 'OfferDetailDialogController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('offer', {
            abstract: true,
            parent: 'app'
        });
    }
})();
