(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('faq', {
            parent: 'app',
            url: '/',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/faq/faq.html',
                    controller: 'FaqController',
                    controllerAs: 'vm'
                }
            }
        });
    }
})();
