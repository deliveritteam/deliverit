(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('contact', {
            parent: 'app',
            url: '/',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/contact/contact.html',
                    controller: 'ContactController',
                    controllerAs: 'vm'
                }
            }
        });
    }
})();
