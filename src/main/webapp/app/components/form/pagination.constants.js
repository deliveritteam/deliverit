(function() {
    'use strict';

    angular
        .module('deliveritApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
