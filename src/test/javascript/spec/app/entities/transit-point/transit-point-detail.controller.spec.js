'use strict';

describe('Controller Tests', function() {

    describe('TransitPoint Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockTransitPoint, MockShipmentOffer, MockPlace;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockTransitPoint = jasmine.createSpy('MockTransitPoint');
            MockShipmentOffer = jasmine.createSpy('MockShipmentOffer');
            MockPlace = jasmine.createSpy('MockPlace');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'TransitPoint': MockTransitPoint,
                'ShipmentOffer': MockShipmentOffer,
                'Place': MockPlace
            };
            createController = function() {
                $injector.get('$controller')("TransitPointDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'deliveritApp:transitPointUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
