'use strict';

describe('Controller Tests', function() {

    describe('Pack Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPack, MockPackageSlot, MockTransitPoint, MockShipmentOfferSubmition;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPack = jasmine.createSpy('MockPack');
            MockPackageSlot = jasmine.createSpy('MockPackageSlot');
            MockTransitPoint = jasmine.createSpy('MockTransitPoint');
            MockShipmentOfferSubmition = jasmine.createSpy('MockShipmentOfferSubmition');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Pack': MockPack,
                'PackageSlot': MockPackageSlot,
                'TransitPoint': MockTransitPoint,
                'ShipmentOfferSubmition': MockShipmentOfferSubmition
            };
            createController = function() {
                $injector.get('$controller')("PackDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'deliveritApp:packUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
