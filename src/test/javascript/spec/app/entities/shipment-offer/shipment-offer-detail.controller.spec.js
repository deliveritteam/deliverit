'use strict';

describe('Controller Tests', function() {

    describe('ShipmentOffer Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockShipmentOffer, MockUser, MockTransitPoint, MockPackageSlot;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockShipmentOffer = jasmine.createSpy('MockShipmentOffer');
            MockUser = jasmine.createSpy('MockUser');
            MockTransitPoint = jasmine.createSpy('MockTransitPoint');
            MockPackageSlot = jasmine.createSpy('MockPackageSlot');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'ShipmentOffer': MockShipmentOffer,
                'User': MockUser,
                'TransitPoint': MockTransitPoint,
                'PackageSlot': MockPackageSlot
            };
            createController = function() {
                $injector.get('$controller')("ShipmentOfferDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'deliveritApp:shipmentOfferUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
