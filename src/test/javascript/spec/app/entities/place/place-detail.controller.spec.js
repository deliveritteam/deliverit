'use strict';

describe('Controller Tests', function() {

    describe('Place Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPlace, MockUser, MockTransitPoint;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPlace = jasmine.createSpy('MockPlace');
            MockUser = jasmine.createSpy('MockUser');
            MockTransitPoint = jasmine.createSpy('MockTransitPoint');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Place': MockPlace,
                'User': MockUser,
                'TransitPoint': MockTransitPoint
            };
            createController = function() {
                $injector.get('$controller')("PlaceDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'deliveritApp:placeUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
