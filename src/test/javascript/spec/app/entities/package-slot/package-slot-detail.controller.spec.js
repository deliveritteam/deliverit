'use strict';

describe('Controller Tests', function() {

    describe('PackageSlot Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPackageSlot, MockShipmentOffer;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPackageSlot = jasmine.createSpy('MockPackageSlot');
            MockShipmentOffer = jasmine.createSpy('MockShipmentOffer');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'PackageSlot': MockPackageSlot,
                'ShipmentOffer': MockShipmentOffer
            };
            createController = function() {
                $injector.get('$controller')("PackageSlotDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'deliveritApp:packageSlotUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
