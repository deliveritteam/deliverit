'use strict';

describe('Controller Tests', function() {

    describe('ShipmentOfferSubmition Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockShipmentOfferSubmition, MockTransitPoint, MockUser, MockPack;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockShipmentOfferSubmition = jasmine.createSpy('MockShipmentOfferSubmition');
            MockTransitPoint = jasmine.createSpy('MockTransitPoint');
            MockUser = jasmine.createSpy('MockUser');
            MockPack = jasmine.createSpy('MockPack');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'ShipmentOfferSubmition': MockShipmentOfferSubmition,
                'TransitPoint': MockTransitPoint,
                'User': MockUser,
                'Pack': MockPack
            };
            createController = function() {
                $injector.get('$controller')("ShipmentOfferSubmitionDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'deliveritApp:shipmentOfferSubmitionUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
