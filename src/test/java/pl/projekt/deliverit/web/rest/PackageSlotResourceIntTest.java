package pl.projekt.deliverit.web.rest;

import pl.projekt.deliverit.DeliveritApp;
import pl.projekt.deliverit.domain.PackageSlot;
import pl.projekt.deliverit.repository.PackageSlotRepository;
import pl.projekt.deliverit.service.PackageSlotService;
import pl.projekt.deliverit.repository.search.PackageSlotSearchRepository;
import pl.projekt.deliverit.web.rest.dto.PackageSlotDTO;
import pl.projekt.deliverit.web.rest.mapper.PackageSlotMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PackageSlotResource REST controller.
 *
 * @see PackageSlotResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DeliveritApp.class)
@WebAppConfiguration
@IntegrationTest
public class PackageSlotResourceIntTest {

    private static final String DEFAULT_SIZE = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    private static final String UPDATED_SIZE = "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBB";

    private static final Float DEFAULT_PRICE = 1F;
    private static final Float UPDATED_PRICE = 2F;

    private static final Integer DEFAULT_QUANTITY = 1;
    private static final Integer UPDATED_QUANTITY = 2;

    private static final Integer DEFAULT_REMAINING_QUANTITY = 1;
    private static final Integer UPDATED_REMAINING_QUANTITY = 2;

    @Inject
    private PackageSlotRepository packageSlotRepository;

    @Inject
    private PackageSlotMapper packageSlotMapper;

    @Inject
    private PackageSlotService packageSlotService;

    @Inject
    private PackageSlotSearchRepository packageSlotSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPackageSlotMockMvc;

    private PackageSlot packageSlot;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PackageSlotResource packageSlotResource = new PackageSlotResource();
        ReflectionTestUtils.setField(packageSlotResource, "packageSlotService", packageSlotService);
        ReflectionTestUtils.setField(packageSlotResource, "packageSlotMapper", packageSlotMapper);
        this.restPackageSlotMockMvc = MockMvcBuilders.standaloneSetup(packageSlotResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        packageSlotSearchRepository.deleteAll();
        packageSlot = new PackageSlot();
        packageSlot.setSize(DEFAULT_SIZE);
        packageSlot.setPrice(DEFAULT_PRICE);
        packageSlot.setQuantity(DEFAULT_QUANTITY);
        packageSlot.setRemainingQuantity(DEFAULT_REMAINING_QUANTITY);
    }

    @Test
    @Transactional
    public void createPackageSlot() throws Exception {
        int databaseSizeBeforeCreate = packageSlotRepository.findAll().size();

        // Create the PackageSlot
        PackageSlotDTO packageSlotDTO = packageSlotMapper.packageSlotToPackageSlotDTO(packageSlot);

        restPackageSlotMockMvc.perform(post("/api/package-slots")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(packageSlotDTO)))
                .andExpect(status().isCreated());

        // Validate the PackageSlot in the database
        List<PackageSlot> packageSlots = packageSlotRepository.findAll();
        assertThat(packageSlots).hasSize(databaseSizeBeforeCreate + 1);
        PackageSlot testPackageSlot = packageSlots.get(packageSlots.size() - 1);
        assertThat(testPackageSlot.getSize()).isEqualTo(DEFAULT_SIZE);
        assertThat(testPackageSlot.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testPackageSlot.getQuantity()).isEqualTo(DEFAULT_QUANTITY);
        assertThat(testPackageSlot.getRemainingQuantity()).isEqualTo(DEFAULT_REMAINING_QUANTITY);

        // Validate the PackageSlot in ElasticSearch
        PackageSlot packageSlotEs = packageSlotSearchRepository.findOne(testPackageSlot.getId());
        assertThat(packageSlotEs).isEqualToComparingFieldByField(testPackageSlot);
    }

    @Test
    @Transactional
    public void checkSizeIsRequired() throws Exception {
        int databaseSizeBeforeTest = packageSlotRepository.findAll().size();
        // set the field null
        packageSlot.setSize(null);

        // Create the PackageSlot, which fails.
        PackageSlotDTO packageSlotDTO = packageSlotMapper.packageSlotToPackageSlotDTO(packageSlot);

        restPackageSlotMockMvc.perform(post("/api/package-slots")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(packageSlotDTO)))
                .andExpect(status().isBadRequest());

        List<PackageSlot> packageSlots = packageSlotRepository.findAll();
        assertThat(packageSlots).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRemainingQuantityIsRequired() throws Exception {
        int databaseSizeBeforeTest = packageSlotRepository.findAll().size();
        // set the field null
        packageSlot.setRemainingQuantity(null);

        // Create the PackageSlot, which fails.
        PackageSlotDTO packageSlotDTO = packageSlotMapper.packageSlotToPackageSlotDTO(packageSlot);

        restPackageSlotMockMvc.perform(post("/api/package-slots")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(packageSlotDTO)))
                .andExpect(status().isBadRequest());

        List<PackageSlot> packageSlots = packageSlotRepository.findAll();
        assertThat(packageSlots).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPackageSlots() throws Exception {
        // Initialize the database
        packageSlotRepository.saveAndFlush(packageSlot);

        // Get all the packageSlots
        restPackageSlotMockMvc.perform(get("/api/package-slots?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(packageSlot.getId().intValue())))
                .andExpect(jsonPath("$.[*].size").value(hasItem(DEFAULT_SIZE.toString())))
                .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.doubleValue())))
                .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY)))
                .andExpect(jsonPath("$.[*].remainingQuantity").value(hasItem(DEFAULT_REMAINING_QUANTITY)));
    }

    @Test
    @Transactional
    public void getPackageSlot() throws Exception {
        // Initialize the database
        packageSlotRepository.saveAndFlush(packageSlot);

        // Get the packageSlot
        restPackageSlotMockMvc.perform(get("/api/package-slots/{id}", packageSlot.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(packageSlot.getId().intValue()))
            .andExpect(jsonPath("$.size").value(DEFAULT_SIZE.toString()))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.doubleValue()))
            .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY))
            .andExpect(jsonPath("$.remainingQuantity").value(DEFAULT_REMAINING_QUANTITY));
    }

    @Test
    @Transactional
    public void getNonExistingPackageSlot() throws Exception {
        // Get the packageSlot
        restPackageSlotMockMvc.perform(get("/api/package-slots/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePackageSlot() throws Exception {
        // Initialize the database
        packageSlotRepository.saveAndFlush(packageSlot);
        packageSlotSearchRepository.save(packageSlot);
        int databaseSizeBeforeUpdate = packageSlotRepository.findAll().size();

        // Update the packageSlot
        PackageSlot updatedPackageSlot = new PackageSlot();
        updatedPackageSlot.setId(packageSlot.getId());
        updatedPackageSlot.setSize(UPDATED_SIZE);
        updatedPackageSlot.setPrice(UPDATED_PRICE);
        updatedPackageSlot.setQuantity(UPDATED_QUANTITY);
        updatedPackageSlot.setRemainingQuantity(UPDATED_REMAINING_QUANTITY);
        PackageSlotDTO packageSlotDTO = packageSlotMapper.packageSlotToPackageSlotDTO(updatedPackageSlot);

        restPackageSlotMockMvc.perform(put("/api/package-slots")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(packageSlotDTO)))
                .andExpect(status().isOk());

        // Validate the PackageSlot in the database
        List<PackageSlot> packageSlots = packageSlotRepository.findAll();
        assertThat(packageSlots).hasSize(databaseSizeBeforeUpdate);
        PackageSlot testPackageSlot = packageSlots.get(packageSlots.size() - 1);
        assertThat(testPackageSlot.getSize()).isEqualTo(UPDATED_SIZE);
        assertThat(testPackageSlot.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testPackageSlot.getQuantity()).isEqualTo(UPDATED_QUANTITY);
        assertThat(testPackageSlot.getRemainingQuantity()).isEqualTo(UPDATED_REMAINING_QUANTITY);

        // Validate the PackageSlot in ElasticSearch
        PackageSlot packageSlotEs = packageSlotSearchRepository.findOne(testPackageSlot.getId());
        assertThat(packageSlotEs).isEqualToComparingFieldByField(testPackageSlot);
    }

    @Test
    @Transactional
    public void deletePackageSlot() throws Exception {
        // Initialize the database
        packageSlotRepository.saveAndFlush(packageSlot);
        packageSlotSearchRepository.save(packageSlot);
        int databaseSizeBeforeDelete = packageSlotRepository.findAll().size();

        // Get the packageSlot
        restPackageSlotMockMvc.perform(delete("/api/package-slots/{id}", packageSlot.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean packageSlotExistsInEs = packageSlotSearchRepository.exists(packageSlot.getId());
        assertThat(packageSlotExistsInEs).isFalse();

        // Validate the database is empty
        List<PackageSlot> packageSlots = packageSlotRepository.findAll();
        assertThat(packageSlots).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPackageSlot() throws Exception {
        // Initialize the database
        packageSlotRepository.saveAndFlush(packageSlot);
        packageSlotSearchRepository.save(packageSlot);

        // Search the packageSlot
        restPackageSlotMockMvc.perform(get("/api/_search/package-slots?query=id:" + packageSlot.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(packageSlot.getId().intValue())))
            .andExpect(jsonPath("$.[*].size").value(hasItem(DEFAULT_SIZE.toString())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY)))
            .andExpect(jsonPath("$.[*].remainingQuantity").value(hasItem(DEFAULT_REMAINING_QUANTITY)));
    }
}
