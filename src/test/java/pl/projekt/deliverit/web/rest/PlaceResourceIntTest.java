package pl.projekt.deliverit.web.rest;

import pl.projekt.deliverit.DeliveritApp;
import pl.projekt.deliverit.domain.Place;
import pl.projekt.deliverit.repository.PlaceRepository;
import pl.projekt.deliverit.service.PlaceService;
import pl.projekt.deliverit.repository.search.PlaceSearchRepository;
import pl.projekt.deliverit.web.rest.dto.PlaceDTO;
import pl.projekt.deliverit.web.rest.mapper.PlaceMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import pl.projekt.deliverit.domain.enumeration.PlaceStatus;

/**
 * Test class for the PlaceResource REST controller.
 *
 * @see PlaceResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DeliveritApp.class)
@WebAppConfiguration
@IntegrationTest
public class PlaceResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_CITY = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB";

    private static final Float DEFAULT_LATITUDE = 1F;
    private static final Float UPDATED_LATITUDE = 2F;

    private static final Float DEFAULT_LONGITUDE = 1F;
    private static final Float UPDATED_LONGITUDE = 2F;
    private static final String DEFAULT_ADDRESS = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATION_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATION_DATE_STR = dateTimeFormatter.format(DEFAULT_CREATION_DATE);

    private static final PlaceStatus DEFAULT_STATUS = PlaceStatus.disapproved;
    private static final PlaceStatus UPDATED_STATUS = PlaceStatus.approved;
    private static final String DEFAULT_POST_CODE = "AAAAAAAAAA";
    private static final String UPDATED_POST_CODE = "BBBBBBBBBB";

    @Inject
    private PlaceRepository placeRepository;

    @Inject
    private PlaceMapper placeMapper;

    @Inject
    private PlaceService placeService;

    @Inject
    private PlaceSearchRepository placeSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPlaceMockMvc;

    private Place place;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PlaceResource placeResource = new PlaceResource();
        ReflectionTestUtils.setField(placeResource, "placeService", placeService);
        ReflectionTestUtils.setField(placeResource, "placeMapper", placeMapper);
        this.restPlaceMockMvc = MockMvcBuilders.standaloneSetup(placeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        placeSearchRepository.deleteAll();
        place = new Place();
        place.setName(DEFAULT_NAME);
        place.setCity(DEFAULT_CITY);
        place.setLatitude(DEFAULT_LATITUDE);
        place.setLongitude(DEFAULT_LONGITUDE);
        place.setAddress(DEFAULT_ADDRESS);
        place.setCreationDate(DEFAULT_CREATION_DATE);
        place.setStatus(DEFAULT_STATUS);
        place.setPostCode(DEFAULT_POST_CODE);
    }

    @Test
    @Transactional
    public void createPlace() throws Exception {
        int databaseSizeBeforeCreate = placeRepository.findAll().size();

        // Create the Place
        PlaceDTO placeDTO = placeMapper.placeToPlaceDTO(place);

        restPlaceMockMvc.perform(post("/api/places")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(placeDTO)))
                .andExpect(status().isCreated());

        // Validate the Place in the database
        List<Place> places = placeRepository.findAll();
        assertThat(places).hasSize(databaseSizeBeforeCreate + 1);
        Place testPlace = places.get(places.size() - 1);
        assertThat(testPlace.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPlace.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testPlace.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testPlace.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testPlace.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testPlace.getCreationDate()).isNotEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testPlace.getStatus()).isNotEqualTo(DEFAULT_STATUS);
        assertThat(testPlace.getPostCode()).isEqualTo(DEFAULT_POST_CODE);


    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = placeRepository.findAll().size();
        // set the field null
        place.setName(null);

        // Create the Place, which fails.
        PlaceDTO placeDTO = placeMapper.placeToPlaceDTO(place);

        restPlaceMockMvc.perform(post("/api/places")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(placeDTO)))
                .andExpect(status().isBadRequest());

        List<Place> places = placeRepository.findAll();
        assertThat(places).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCityIsRequired() throws Exception {
        int databaseSizeBeforeTest = placeRepository.findAll().size();
        // set the field null
        place.setCity(null);

        // Create the Place, which fails.
        PlaceDTO placeDTO = placeMapper.placeToPlaceDTO(place);

        restPlaceMockMvc.perform(post("/api/places")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(placeDTO)))
                .andExpect(status().isBadRequest());

        List<Place> places = placeRepository.findAll();
        assertThat(places).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAddressIsRequired() throws Exception {
        int databaseSizeBeforeTest = placeRepository.findAll().size();
        // set the field null
        place.setAddress(null);

        // Create the Place, which fails.
        PlaceDTO placeDTO = placeMapper.placeToPlaceDTO(place);

        restPlaceMockMvc.perform(post("/api/places")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(placeDTO)))
                .andExpect(status().isBadRequest());

        List<Place> places = placeRepository.findAll();
        assertThat(places).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPostCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = placeRepository.findAll().size();
        // set the field null
        place.setPostCode(null);

        // Create the Place, which fails.
        PlaceDTO placeDTO = placeMapper.placeToPlaceDTO(place);

        restPlaceMockMvc.perform(post("/api/places")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(placeDTO)))
                .andExpect(status().isBadRequest());

        List<Place> places = placeRepository.findAll();
        assertThat(places).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPlaces() throws Exception {
        // Initialize the database
        placeRepository.saveAndFlush(place);

        // Get all the places
        restPlaceMockMvc.perform(get("/api/places?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(place.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
                .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
                .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
                .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
                .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE_STR)))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
                .andExpect(jsonPath("$.[*].postCode").value(hasItem(DEFAULT_POST_CODE.toString())));
    }

    @Test
    @Transactional
    public void getPlace() throws Exception {
        // Initialize the database
        placeRepository.saveAndFlush(place);

        // Get the place
        restPlaceMockMvc.perform(get("/api/places/{id}", place.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(place.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE_STR))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.postCode").value(DEFAULT_POST_CODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPlace() throws Exception {
        // Get the place
        restPlaceMockMvc.perform(get("/api/places/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePlace() throws Exception {
        // Initialize the database
        placeRepository.saveAndFlush(place);
        placeSearchRepository.save(place);
        int databaseSizeBeforeUpdate = placeRepository.findAll().size();

        // Update the place
        Place updatedPlace = new Place();
        updatedPlace.setId(place.getId());
        updatedPlace.setName(UPDATED_NAME);
        updatedPlace.setCity(UPDATED_CITY);
        updatedPlace.setLatitude(UPDATED_LATITUDE);
        updatedPlace.setLongitude(UPDATED_LONGITUDE);
        updatedPlace.setAddress(UPDATED_ADDRESS);
        updatedPlace.setCreationDate(UPDATED_CREATION_DATE);
        updatedPlace.setStatus(UPDATED_STATUS);
        updatedPlace.setPostCode(UPDATED_POST_CODE);
        PlaceDTO placeDTO = placeMapper.placeToPlaceDTO(updatedPlace);

        restPlaceMockMvc.perform(put("/api/places")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(placeDTO)))
                .andExpect(status().isOk());

        // Validate the Place in the database
        List<Place> places = placeRepository.findAll();
        assertThat(places).hasSize(databaseSizeBeforeUpdate);
        Place testPlace = places.get(places.size() - 1);
        assertThat(testPlace.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPlace.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testPlace.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testPlace.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testPlace.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testPlace.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testPlace.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testPlace.getPostCode()).isEqualTo(UPDATED_POST_CODE);

        // Validate the Place in ElasticSearch
        Place placeEs = placeSearchRepository.findOne(testPlace.getId());
        assertThat(placeEs).isEqualToComparingFieldByField(testPlace);
    }

    @Test
    @Transactional
    public void deletePlace() throws Exception {
        // Initialize the database
        placeRepository.saveAndFlush(place);
        placeSearchRepository.save(place);
        int databaseSizeBeforeDelete = placeRepository.findAll().size();

        // Get the place
        restPlaceMockMvc.perform(delete("/api/places/{id}", place.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean placeExistsInEs = placeSearchRepository.exists(place.getId());
        assertThat(placeExistsInEs).isFalse();

        // Validate the database is empty
        List<Place> places = placeRepository.findAll();
        assertThat(places).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPlace() throws Exception {
        // Initialize the database
        placeRepository.saveAndFlush(place);
        placeSearchRepository.save(place);

        // Search the place
        restPlaceMockMvc.perform(get("/api/_search/places?query=id:" + place.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(place.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE_STR)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].postCode").value(hasItem(DEFAULT_POST_CODE.toString())));
    }
}
