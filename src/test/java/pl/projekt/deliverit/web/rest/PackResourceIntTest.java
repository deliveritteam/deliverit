package pl.projekt.deliverit.web.rest;

import pl.projekt.deliverit.DeliveritApp;
import pl.projekt.deliverit.domain.Pack;
import pl.projekt.deliverit.repository.PackRepository;
import pl.projekt.deliverit.service.PackService;
import pl.projekt.deliverit.repository.search.PackSearchRepository;
import pl.projekt.deliverit.web.rest.dto.PackDTO;
import pl.projekt.deliverit.web.rest.mapper.PackMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PackResource REST controller.
 *
 * @see PackResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DeliveritApp.class)
@WebAppConfiguration
@IntegrationTest
public class PackResourceIntTest {

    private static final String DEFAULT_PACKAGE_CODE = "AAAAA";
    private static final String UPDATED_PACKAGE_CODE = "BBBBB";

    @Inject
    private PackRepository packRepository;

    @Inject
    private PackMapper packMapper;

    @Inject
    private PackService packService;

    @Inject
    private PackSearchRepository packSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPackMockMvc;

    private Pack pack;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PackResource packResource = new PackResource();
        ReflectionTestUtils.setField(packResource, "packService", packService);
        ReflectionTestUtils.setField(packResource, "packMapper", packMapper);
        this.restPackMockMvc = MockMvcBuilders.standaloneSetup(packResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        packSearchRepository.deleteAll();
        pack = new Pack();
        pack.setPackageCode(DEFAULT_PACKAGE_CODE);
    }

    @Test
    @Transactional
    public void createPack() throws Exception {
        int databaseSizeBeforeCreate = packRepository.findAll().size();

        // Create the Pack
        PackDTO packDTO = packMapper.packToPackDTO(pack);

        restPackMockMvc.perform(post("/api/packs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(packDTO)))
                .andExpect(status().isCreated());

        // Validate the Pack in the database
        List<Pack> packs = packRepository.findAll();
        assertThat(packs).hasSize(databaseSizeBeforeCreate + 1);
        Pack testPack = packs.get(packs.size() - 1);
        assertThat(testPack.getPackageCode()).isEqualTo(DEFAULT_PACKAGE_CODE);

        // Validate the Pack in ElasticSearch
        Pack packEs = packSearchRepository.findOne(testPack.getId());
        assertThat(packEs).isEqualToComparingFieldByField(testPack);
    }

    @Test
    @Transactional
    public void getAllPacks() throws Exception {
        // Initialize the database
        packRepository.saveAndFlush(pack);

        // Get all the packs
        restPackMockMvc.perform(get("/api/packs?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(pack.getId().intValue())))
                .andExpect(jsonPath("$.[*].packageCode").value(hasItem(DEFAULT_PACKAGE_CODE.toString())));
    }

    @Test
    @Transactional
    public void getPack() throws Exception {
        // Initialize the database
        packRepository.saveAndFlush(pack);

        // Get the pack
        restPackMockMvc.perform(get("/api/packs/{id}", pack.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(pack.getId().intValue()))
            .andExpect(jsonPath("$.packageCode").value(DEFAULT_PACKAGE_CODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPack() throws Exception {
        // Get the pack
        restPackMockMvc.perform(get("/api/packs/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePack() throws Exception {
        // Initialize the database
        packRepository.saveAndFlush(pack);
        packSearchRepository.save(pack);
        int databaseSizeBeforeUpdate = packRepository.findAll().size();

        // Update the pack
        Pack updatedPack = new Pack();
        updatedPack.setId(pack.getId());
        updatedPack.setPackageCode(UPDATED_PACKAGE_CODE);
        PackDTO packDTO = packMapper.packToPackDTO(updatedPack);

        restPackMockMvc.perform(put("/api/packs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(packDTO)))
                .andExpect(status().isOk());

        // Validate the Pack in the database
        List<Pack> packs = packRepository.findAll();
        assertThat(packs).hasSize(databaseSizeBeforeUpdate);
        Pack testPack = packs.get(packs.size() - 1);
        assertThat(testPack.getPackageCode()).isEqualTo(UPDATED_PACKAGE_CODE);

        // Validate the Pack in ElasticSearch
        Pack packEs = packSearchRepository.findOne(testPack.getId());
        assertThat(packEs).isEqualToComparingFieldByField(testPack);
    }

    @Test
    @Transactional
    public void deletePack() throws Exception {
        // Initialize the database
        packRepository.saveAndFlush(pack);
        packSearchRepository.save(pack);
        int databaseSizeBeforeDelete = packRepository.findAll().size();

        // Get the pack
        restPackMockMvc.perform(delete("/api/packs/{id}", pack.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean packExistsInEs = packSearchRepository.exists(pack.getId());
        assertThat(packExistsInEs).isFalse();

        // Validate the database is empty
        List<Pack> packs = packRepository.findAll();
        assertThat(packs).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPack() throws Exception {
        // Initialize the database
        packRepository.saveAndFlush(pack);
        packSearchRepository.save(pack);

        // Search the pack
        restPackMockMvc.perform(get("/api/_search/packs?query=id:" + pack.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pack.getId().intValue())))
            .andExpect(jsonPath("$.[*].packageCode").value(hasItem(DEFAULT_PACKAGE_CODE.toString())));
    }
}
