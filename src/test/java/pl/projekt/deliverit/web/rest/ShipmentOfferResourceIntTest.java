package pl.projekt.deliverit.web.rest;

import pl.projekt.deliverit.DeliveritApp;
import pl.projekt.deliverit.domain.ShipmentOffer;
import pl.projekt.deliverit.repository.ShipmentOfferRepository;
import pl.projekt.deliverit.service.ShipmentOfferService;
import pl.projekt.deliverit.repository.search.ShipmentOfferSearchRepository;
import pl.projekt.deliverit.web.rest.dto.ShipmentOfferDTO;
import pl.projekt.deliverit.web.rest.mapper.ShipmentOfferMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ShipmentOfferResource REST controller.
 *
 * @see ShipmentOfferResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DeliveritApp.class)
@WebAppConfiguration
@IntegrationTest
public class ShipmentOfferResourceIntTest {


    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    @Inject
    private ShipmentOfferRepository shipmentOfferRepository;

    @Inject
    private ShipmentOfferMapper shipmentOfferMapper;

    @Inject
    private ShipmentOfferService shipmentOfferService;

    @Inject
    private ShipmentOfferSearchRepository shipmentOfferSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restShipmentOfferMockMvc;

    private ShipmentOffer shipmentOffer;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ShipmentOfferResource shipmentOfferResource = new ShipmentOfferResource();
        ReflectionTestUtils.setField(shipmentOfferResource, "shipmentOfferService", shipmentOfferService);
        ReflectionTestUtils.setField(shipmentOfferResource, "shipmentOfferMapper", shipmentOfferMapper);
        this.restShipmentOfferMockMvc = MockMvcBuilders.standaloneSetup(shipmentOfferResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        shipmentOfferSearchRepository.deleteAll();
        shipmentOffer = new ShipmentOffer();
        shipmentOffer.setDate(DEFAULT_DATE);
        shipmentOffer.setDescription(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createShipmentOffer() throws Exception {
        int databaseSizeBeforeCreate = shipmentOfferRepository.findAll().size();

        // Create the ShipmentOffer
        ShipmentOfferDTO shipmentOfferDTO = shipmentOfferMapper.shipmentOfferToShipmentOfferDTO(shipmentOffer);

        restShipmentOfferMockMvc.perform(post("/api/shipment-offers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(shipmentOfferDTO)))
                .andExpect(status().isCreated());

        // Validate the ShipmentOffer in the database
        List<ShipmentOffer> shipmentOffers = shipmentOfferRepository.findAll();
        assertThat(shipmentOffers).hasSize(databaseSizeBeforeCreate + 1);
        ShipmentOffer testShipmentOffer = shipmentOffers.get(shipmentOffers.size() - 1);
        assertThat(testShipmentOffer.getDate()).isNotEqualTo(DEFAULT_DATE);
        assertThat(testShipmentOffer.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);

        // Validate the ShipmentOffer in ElasticSearch
        ShipmentOffer shipmentOfferEs = shipmentOfferSearchRepository.findOne(testShipmentOffer.getId());
        assertThat(shipmentOfferEs).isEqualToComparingFieldByField(testShipmentOffer);
    }

    @Test
    @Transactional
    public void getAllShipmentOffers() throws Exception {
        // Initialize the database
        shipmentOfferRepository.saveAndFlush(shipmentOffer);

        // Get all the shipmentOffers
        restShipmentOfferMockMvc.perform(get("/api/shipment-offers?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(shipmentOffer.getId().intValue())))
                .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getShipmentOffer() throws Exception {
        // Initialize the database
        shipmentOfferRepository.saveAndFlush(shipmentOffer);

        // Get the shipmentOffer
        restShipmentOfferMockMvc.perform(get("/api/shipment-offers/{id}", shipmentOffer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(shipmentOffer.getId().intValue()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingShipmentOffer() throws Exception {
        // Get the shipmentOffer
        restShipmentOfferMockMvc.perform(get("/api/shipment-offers/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateShipmentOffer() throws Exception {
        // Initialize the database
        shipmentOfferRepository.saveAndFlush(shipmentOffer);
        shipmentOfferSearchRepository.save(shipmentOffer);
        int databaseSizeBeforeUpdate = shipmentOfferRepository.findAll().size();

        // Update the shipmentOffer
        ShipmentOffer updatedShipmentOffer = new ShipmentOffer();
        updatedShipmentOffer.setId(shipmentOffer.getId());
        updatedShipmentOffer.setDate(UPDATED_DATE);
        updatedShipmentOffer.setDescription(UPDATED_DESCRIPTION);
        ShipmentOfferDTO shipmentOfferDTO = shipmentOfferMapper.shipmentOfferToShipmentOfferDTO(updatedShipmentOffer);

        restShipmentOfferMockMvc.perform(put("/api/shipment-offers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(shipmentOfferDTO)))
                .andExpect(status().isOk());

        // Validate the ShipmentOffer in the database
        List<ShipmentOffer> shipmentOffers = shipmentOfferRepository.findAll();
        assertThat(shipmentOffers).hasSize(databaseSizeBeforeUpdate);
        ShipmentOffer testShipmentOffer = shipmentOffers.get(shipmentOffers.size() - 1);
        assertThat(testShipmentOffer.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testShipmentOffer.getDescription()).isEqualTo(UPDATED_DESCRIPTION);

        // Validate the ShipmentOffer in ElasticSearch
        ShipmentOffer shipmentOfferEs = shipmentOfferSearchRepository.findOne(testShipmentOffer.getId());
        assertThat(shipmentOfferEs).isEqualToComparingFieldByField(testShipmentOffer);
    }

    @Test
    @Transactional
    public void deleteShipmentOffer() throws Exception {
        // Initialize the database
        shipmentOfferRepository.saveAndFlush(shipmentOffer);
        shipmentOfferSearchRepository.save(shipmentOffer);
        int databaseSizeBeforeDelete = shipmentOfferRepository.findAll().size();

        // Get the shipmentOffer
        restShipmentOfferMockMvc.perform(delete("/api/shipment-offers/{id}", shipmentOffer.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean shipmentOfferExistsInEs = shipmentOfferSearchRepository.exists(shipmentOffer.getId());
        assertThat(shipmentOfferExistsInEs).isFalse();

        // Validate the database is empty
        List<ShipmentOffer> shipmentOffers = shipmentOfferRepository.findAll();
        assertThat(shipmentOffers).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchShipmentOffer() throws Exception {
        // Initialize the database
        shipmentOfferRepository.saveAndFlush(shipmentOffer);
        shipmentOfferSearchRepository.save(shipmentOffer);

        // Search the shipmentOffer
        restShipmentOfferMockMvc.perform(get("/api/_search/shipment-offers?query=id:" + shipmentOffer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shipmentOffer.getId().intValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
}
