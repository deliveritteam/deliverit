package pl.projekt.deliverit.web.rest;

import pl.projekt.deliverit.DeliveritApp;
import pl.projekt.deliverit.domain.TransitPoint;
import pl.projekt.deliverit.repository.TransitPointRepository;
import pl.projekt.deliverit.service.TransitPointService;
import pl.projekt.deliverit.repository.search.TransitPointSearchRepository;
import pl.projekt.deliverit.web.rest.dto.TransitPointDTO;
import pl.projekt.deliverit.web.rest.mapper.TransitPointMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the TransitPointResource REST controller.
 *
 * @see TransitPointResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DeliveritApp.class)
@WebAppConfiguration
@IntegrationTest
public class TransitPointResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_CITY = "AAAAA";
    private static final String UPDATED_CITY = "BBBBB";

    private static final ZonedDateTime DEFAULT_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_STR = dateTimeFormatter.format(DEFAULT_DATE);

    @Inject
    private TransitPointRepository transitPointRepository;

    @Inject
    private TransitPointMapper transitPointMapper;

    @Inject
    private TransitPointService transitPointService;

    @Inject
    private TransitPointSearchRepository transitPointSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTransitPointMockMvc;

    private TransitPoint transitPoint;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TransitPointResource transitPointResource = new TransitPointResource();
        ReflectionTestUtils.setField(transitPointResource, "transitPointService", transitPointService);
        ReflectionTestUtils.setField(transitPointResource, "transitPointMapper", transitPointMapper);
        this.restTransitPointMockMvc = MockMvcBuilders.standaloneSetup(transitPointResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        transitPointSearchRepository.deleteAll();
        transitPoint = new TransitPoint();
        transitPoint.setCity(DEFAULT_CITY);
        transitPoint.setDate(DEFAULT_DATE);
    }

    @Test
    @Transactional
    public void createTransitPoint() throws Exception {
        int databaseSizeBeforeCreate = transitPointRepository.findAll().size();

        // Create the TransitPoint
        TransitPointDTO transitPointDTO = transitPointMapper.transitPointToTransitPointDTO(transitPoint);

        restTransitPointMockMvc.perform(post("/api/transit-points")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(transitPointDTO)))
                .andExpect(status().isCreated());

        // Validate the TransitPoint in the database
        List<TransitPoint> transitPoints = transitPointRepository.findAll();
        assertThat(transitPoints).hasSize(databaseSizeBeforeCreate + 1);
        TransitPoint testTransitPoint = transitPoints.get(transitPoints.size() - 1);
        assertThat(testTransitPoint.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testTransitPoint.getDate()).isEqualTo(DEFAULT_DATE);

        // Validate the TransitPoint in ElasticSearch
        TransitPoint transitPointEs = transitPointSearchRepository.findOne(testTransitPoint.getId());
        assertThat(transitPointEs).isEqualToComparingFieldByField(testTransitPoint);
    }

    @Test
    @Transactional
    public void getAllTransitPoints() throws Exception {
        // Initialize the database
        transitPointRepository.saveAndFlush(transitPoint);

        // Get all the transitPoints
        restTransitPointMockMvc.perform(get("/api/transit-points?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(transitPoint.getId().intValue())))
                .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
                .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE_STR)));
    }

    @Test
    @Transactional
    public void getTransitPoint() throws Exception {
        // Initialize the database
        transitPointRepository.saveAndFlush(transitPoint);

        // Get the transitPoint
        restTransitPointMockMvc.perform(get("/api/transit-points/{id}", transitPoint.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(transitPoint.getId().intValue()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE_STR));
    }

    @Test
    @Transactional
    public void getNonExistingTransitPoint() throws Exception {
        // Get the transitPoint
        restTransitPointMockMvc.perform(get("/api/transit-points/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTransitPoint() throws Exception {
        // Initialize the database
        transitPointRepository.saveAndFlush(transitPoint);
        transitPointSearchRepository.save(transitPoint);
        int databaseSizeBeforeUpdate = transitPointRepository.findAll().size();

        // Update the transitPoint
        TransitPoint updatedTransitPoint = new TransitPoint();
        updatedTransitPoint.setId(transitPoint.getId());
        updatedTransitPoint.setCity(UPDATED_CITY);
        updatedTransitPoint.setDate(UPDATED_DATE);
        TransitPointDTO transitPointDTO = transitPointMapper.transitPointToTransitPointDTO(updatedTransitPoint);

        restTransitPointMockMvc.perform(put("/api/transit-points")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(transitPointDTO)))
                .andExpect(status().isOk());

        // Validate the TransitPoint in the database
        List<TransitPoint> transitPoints = transitPointRepository.findAll();
        assertThat(transitPoints).hasSize(databaseSizeBeforeUpdate);
        TransitPoint testTransitPoint = transitPoints.get(transitPoints.size() - 1);
        assertThat(testTransitPoint.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testTransitPoint.getDate()).isEqualTo(UPDATED_DATE);

        // Validate the TransitPoint in ElasticSearch
        TransitPoint transitPointEs = transitPointSearchRepository.findOne(testTransitPoint.getId());
        assertThat(transitPointEs).isEqualToComparingFieldByField(testTransitPoint);
    }

    @Test
    @Transactional
    public void deleteTransitPoint() throws Exception {
        // Initialize the database
        transitPointRepository.saveAndFlush(transitPoint);
        transitPointSearchRepository.save(transitPoint);
        int databaseSizeBeforeDelete = transitPointRepository.findAll().size();

        // Get the transitPoint
        restTransitPointMockMvc.perform(delete("/api/transit-points/{id}", transitPoint.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean transitPointExistsInEs = transitPointSearchRepository.exists(transitPoint.getId());
        assertThat(transitPointExistsInEs).isFalse();

        // Validate the database is empty
        List<TransitPoint> transitPoints = transitPointRepository.findAll();
        assertThat(transitPoints).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTransitPoint() throws Exception {
        // Initialize the database
        transitPointRepository.saveAndFlush(transitPoint);
        transitPointSearchRepository.save(transitPoint);

        // Search the transitPoint
        restTransitPointMockMvc.perform(get("/api/_search/transit-points?query=id:" + transitPoint.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transitPoint.getId().intValue())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE_STR)));
    }
}
