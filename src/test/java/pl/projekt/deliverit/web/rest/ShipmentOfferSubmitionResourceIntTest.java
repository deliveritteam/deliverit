package pl.projekt.deliverit.web.rest;

import pl.projekt.deliverit.DeliveritApp;
import pl.projekt.deliverit.domain.ShipmentOfferSubmition;
import pl.projekt.deliverit.repository.ShipmentOfferSubmitionRepository;
import pl.projekt.deliverit.service.ShipmentOfferSubmitionService;
import pl.projekt.deliverit.repository.search.ShipmentOfferSubmitionSearchRepository;
import pl.projekt.deliverit.web.rest.dto.ShipmentOfferSubmitionDTO;
import pl.projekt.deliverit.web.rest.mapper.ShipmentOfferSubmitionMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import pl.projekt.deliverit.domain.enumeration.Status;

/**
 * Test class for the ShipmentOfferSubmitionResource REST controller.
 *
 * @see ShipmentOfferSubmitionResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DeliveritApp.class)
@WebAppConfiguration
@IntegrationTest
public class ShipmentOfferSubmitionResourceIntTest {


    private static final Status DEFAULT_STATUS = Status.abandoned;
    private static final Status UPDATED_STATUS = Status.approved;
    private static final String DEFAULT_MESSAGE = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_RECEIP_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_RECEIP_DATE = LocalDate.now(ZoneId.systemDefault());

    @Inject
    private ShipmentOfferSubmitionRepository shipmentOfferSubmitionRepository;

    @Inject
    private ShipmentOfferSubmitionMapper shipmentOfferSubmitionMapper;

    @Inject
    private ShipmentOfferSubmitionService shipmentOfferSubmitionService;

    @Inject
    private ShipmentOfferSubmitionSearchRepository shipmentOfferSubmitionSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restShipmentOfferSubmitionMockMvc;

    private ShipmentOfferSubmition shipmentOfferSubmition;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ShipmentOfferSubmitionResource shipmentOfferSubmitionResource = new ShipmentOfferSubmitionResource();
        ReflectionTestUtils.setField(shipmentOfferSubmitionResource, "shipmentOfferSubmitionService", shipmentOfferSubmitionService);
        ReflectionTestUtils.setField(shipmentOfferSubmitionResource, "shipmentOfferSubmitionMapper", shipmentOfferSubmitionMapper);
        this.restShipmentOfferSubmitionMockMvc = MockMvcBuilders.standaloneSetup(shipmentOfferSubmitionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        shipmentOfferSubmitionSearchRepository.deleteAll();
        shipmentOfferSubmition = new ShipmentOfferSubmition();
        shipmentOfferSubmition.setStatus(DEFAULT_STATUS);
        shipmentOfferSubmition.setMessage(DEFAULT_MESSAGE);
        shipmentOfferSubmition.setDate(DEFAULT_DATE);
        shipmentOfferSubmition.setReceipDate(DEFAULT_RECEIP_DATE);
    }

    @Test
    @Transactional
    public void createShipmentOfferSubmition() throws Exception {
        int databaseSizeBeforeCreate = shipmentOfferSubmitionRepository.findAll().size();

        // Create the ShipmentOfferSubmition
        ShipmentOfferSubmitionDTO shipmentOfferSubmitionDTO = shipmentOfferSubmitionMapper.shipmentOfferSubmitionToShipmentOfferSubmitionDTO(shipmentOfferSubmition);

        restShipmentOfferSubmitionMockMvc.perform(post("/api/shipment-offer-submitions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(shipmentOfferSubmitionDTO)))
                .andExpect(status().isCreated());

        // Validate the ShipmentOfferSubmition in the database
        List<ShipmentOfferSubmition> shipmentOfferSubmitions = shipmentOfferSubmitionRepository.findAll();
        assertThat(shipmentOfferSubmitions).hasSize(databaseSizeBeforeCreate + 1);
        ShipmentOfferSubmition testShipmentOfferSubmition = shipmentOfferSubmitions.get(shipmentOfferSubmitions.size() - 1);
        assertThat(testShipmentOfferSubmition.getStatus()).isNotEqualTo(DEFAULT_STATUS);
        assertThat(testShipmentOfferSubmition.getMessage()).isEqualTo(DEFAULT_MESSAGE);
        assertThat(testShipmentOfferSubmition.getDate()).isNotEqualTo(DEFAULT_DATE);
        assertThat(testShipmentOfferSubmition.getReceipDate()).isEqualTo(DEFAULT_RECEIP_DATE);


    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = shipmentOfferSubmitionRepository.findAll().size();
        // set the field null
        shipmentOfferSubmition.setStatus(null);

        // Create the ShipmentOfferSubmition, which fails.
        ShipmentOfferSubmitionDTO shipmentOfferSubmitionDTO = shipmentOfferSubmitionMapper.shipmentOfferSubmitionToShipmentOfferSubmitionDTO(shipmentOfferSubmition);

        restShipmentOfferSubmitionMockMvc.perform(post("/api/shipment-offer-submitions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(shipmentOfferSubmitionDTO)))
                .andExpect(status().isBadRequest());

        List<ShipmentOfferSubmition> shipmentOfferSubmitions = shipmentOfferSubmitionRepository.findAll();
        assertThat(shipmentOfferSubmitions).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMessageIsRequired() throws Exception {
        int databaseSizeBeforeTest = shipmentOfferSubmitionRepository.findAll().size();
        // set the field null
        shipmentOfferSubmition.setMessage(null);

        // Create the ShipmentOfferSubmition, which fails.
        ShipmentOfferSubmitionDTO shipmentOfferSubmitionDTO = shipmentOfferSubmitionMapper.shipmentOfferSubmitionToShipmentOfferSubmitionDTO(shipmentOfferSubmition);

        restShipmentOfferSubmitionMockMvc.perform(post("/api/shipment-offer-submitions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(shipmentOfferSubmitionDTO)))
                .andExpect(status().isBadRequest());

        List<ShipmentOfferSubmition> shipmentOfferSubmitions = shipmentOfferSubmitionRepository.findAll();
        assertThat(shipmentOfferSubmitions).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkReceipDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = shipmentOfferSubmitionRepository.findAll().size();
        // set the field null
        shipmentOfferSubmition.setReceipDate(null);

        // Create the ShipmentOfferSubmition, which fails.
        ShipmentOfferSubmitionDTO shipmentOfferSubmitionDTO = shipmentOfferSubmitionMapper.shipmentOfferSubmitionToShipmentOfferSubmitionDTO(shipmentOfferSubmition);

        restShipmentOfferSubmitionMockMvc.perform(post("/api/shipment-offer-submitions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(shipmentOfferSubmitionDTO)))
                .andExpect(status().isBadRequest());

        List<ShipmentOfferSubmition> shipmentOfferSubmitions = shipmentOfferSubmitionRepository.findAll();
        assertThat(shipmentOfferSubmitions).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllShipmentOfferSubmitions() throws Exception {
        // Initialize the database
        shipmentOfferSubmitionRepository.saveAndFlush(shipmentOfferSubmition);

        // Get all the shipmentOfferSubmitions
        restShipmentOfferSubmitionMockMvc.perform(get("/api/shipment-offer-submitions?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(shipmentOfferSubmition.getId().intValue())))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
                .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE.toString())))
                .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
                .andExpect(jsonPath("$.[*].receipDate").value(hasItem(DEFAULT_RECEIP_DATE.toString())));
    }

    @Test
    @Transactional
    public void getShipmentOfferSubmition() throws Exception {
        // Initialize the database
        shipmentOfferSubmitionRepository.saveAndFlush(shipmentOfferSubmition);

        // Get the shipmentOfferSubmition
        restShipmentOfferSubmitionMockMvc.perform(get("/api/shipment-offer-submitions/{id}", shipmentOfferSubmition.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(shipmentOfferSubmition.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE.toString()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.receipDate").value(DEFAULT_RECEIP_DATE.toString()));
    }

/*    @Test
    @Transactional
    public void getNonExistingShipmentOfferSubmition() throws Exception {
        // Get the shipmentOfferSubmition
        restShipmentOfferSubmitionMockMvc.perform(get("/api/shipment-offer-submitions/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }*/

    @Test
    @Transactional
    public void updateShipmentOfferSubmition() throws Exception {
        // Initialize the database
        shipmentOfferSubmitionRepository.saveAndFlush(shipmentOfferSubmition);
        shipmentOfferSubmitionSearchRepository.save(shipmentOfferSubmition);
        int databaseSizeBeforeUpdate = shipmentOfferSubmitionRepository.findAll().size();

        // Update the shipmentOfferSubmition
        ShipmentOfferSubmition updatedShipmentOfferSubmition = new ShipmentOfferSubmition();
        updatedShipmentOfferSubmition.setId(shipmentOfferSubmition.getId());
        updatedShipmentOfferSubmition.setStatus(UPDATED_STATUS);
        updatedShipmentOfferSubmition.setMessage(UPDATED_MESSAGE);
        updatedShipmentOfferSubmition.setDate(UPDATED_DATE);
        updatedShipmentOfferSubmition.setReceipDate(UPDATED_RECEIP_DATE);
        ShipmentOfferSubmitionDTO shipmentOfferSubmitionDTO = shipmentOfferSubmitionMapper.shipmentOfferSubmitionToShipmentOfferSubmitionDTO(updatedShipmentOfferSubmition);

        restShipmentOfferSubmitionMockMvc.perform(put("/api/shipment-offer-submitions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(shipmentOfferSubmitionDTO)))
                .andExpect(status().isOk());

        // Validate the ShipmentOfferSubmition in the database
        List<ShipmentOfferSubmition> shipmentOfferSubmitions = shipmentOfferSubmitionRepository.findAll();
        assertThat(shipmentOfferSubmitions).hasSize(databaseSizeBeforeUpdate);
        ShipmentOfferSubmition testShipmentOfferSubmition = shipmentOfferSubmitions.get(shipmentOfferSubmitions.size() - 1);
        assertThat(testShipmentOfferSubmition.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testShipmentOfferSubmition.getMessage()).isEqualTo(UPDATED_MESSAGE);
        assertThat(testShipmentOfferSubmition.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testShipmentOfferSubmition.getReceipDate()).isEqualTo(UPDATED_RECEIP_DATE);

        // Validate the ShipmentOfferSubmition in ElasticSearch
        ShipmentOfferSubmition shipmentOfferSubmitionEs = shipmentOfferSubmitionSearchRepository.findOne(testShipmentOfferSubmition.getId());
        assertThat(shipmentOfferSubmitionEs).isEqualToComparingFieldByField(testShipmentOfferSubmition);
    }

    @Test
    @Transactional
    public void deleteShipmentOfferSubmition() throws Exception {
        // Initialize the database
        shipmentOfferSubmitionRepository.saveAndFlush(shipmentOfferSubmition);
        shipmentOfferSubmitionSearchRepository.save(shipmentOfferSubmition);
        int databaseSizeBeforeDelete = shipmentOfferSubmitionRepository.findAll().size();

        // Get the shipmentOfferSubmition
        restShipmentOfferSubmitionMockMvc.perform(delete("/api/shipment-offer-submitions/{id}", shipmentOfferSubmition.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean shipmentOfferSubmitionExistsInEs = shipmentOfferSubmitionSearchRepository.exists(shipmentOfferSubmition.getId());
        assertThat(shipmentOfferSubmitionExistsInEs).isFalse();

        // Validate the database is empty
        List<ShipmentOfferSubmition> shipmentOfferSubmitions = shipmentOfferSubmitionRepository.findAll();
        assertThat(shipmentOfferSubmitions).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchShipmentOfferSubmition() throws Exception {
        // Initialize the database
        shipmentOfferSubmitionRepository.saveAndFlush(shipmentOfferSubmition);
        shipmentOfferSubmitionSearchRepository.save(shipmentOfferSubmition);

        // Search the shipmentOfferSubmition
        restShipmentOfferSubmitionMockMvc.perform(get("/api/_search/shipment-offer-submitions?query=id:" + shipmentOfferSubmition.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shipmentOfferSubmition.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE.toString())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].receipDate").value(hasItem(DEFAULT_RECEIP_DATE.toString())));
    }
}
